/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 *
 * @author maestrom4
 */
public class Payslip {
//    public String paySlipId;
    public String empId;
    public String Fname;
    public String Lname;
    public String Client;
    public String Project;
   
    public double[] deductions;
    public int hoursWorked;
    public int overtimeRate;
    public String position;
    public double rate;
    Payslip(String empIdP,String FnameP,String LnameP,String ClientP, String ProjectP, double[] deductionsP,int hoursWorkedP, int overtimeRateP,String positionP, double rateP){
//        this.paySlipId = paySlipIdP;
        this.empId = empIdP;
        this.Fname = FnameP;
        this.Lname = LnameP;
        this.Client = ClientP;
        this.Project = ProjectP;
        this.deductions = deductionsP;
        this.hoursWorked = hoursWorkedP;
        this.overtimeRate = overtimeRateP;
        this.position = positionP;
        this.rate = rateP;
    }
    public double grossPay(){
        double grosspay;
        
        return grosspay = (this.hoursWorked*this.rate)+(this.overtimeRate*this.overtimeRate);
        
    }
    public double totalDeductions(){
        double total=0.0;
        int countDeductions = this.deductions.length;
        System.out.println("passhere!*************************************");
        for(int a=0;a<countDeductions;a++){
            total+=this.deductions[a];
        }
        return total;
    }
    public double netPay(){
  
        return this.grossPay()-this.totalDeductions();
    }
}
