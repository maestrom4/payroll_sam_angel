/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;
import java.awt.Toolkit;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.text.DateFormat;
import java.util.Locale;
import java.util.*;
import java.text.*;
//import java.util.dateTime;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.TimeZone;
import javax.swing.JOptionPane;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileOutputStream;
import payroll.pdfwriter;
import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;



/**
 *
 * @author maestrom4
 */
public class mainwindow extends javax.swing.JFrame {
    public Connection conn;
    public String url = "jdbc:mysql://localhost:3306/payrolldb2?useSSL=true";
    public String dbName = "payrolldb2";
    public String driver = "com.mysql.jdbc.Driver";
    public String userNamedb = "root"; 
    public String passworddb = "";
    public String[] tempEmpData = new String[7];
    private String _currentUpdateId= "";
    public String [] tempList_other_id = new String[1]; 
    public boolean check_other_id_exist_var = false;
    public Object [][] tempPayslipDeductions = {}; 
    public String payslipDeductionTblSelected= "";
    public String transferPayslipDeductions = "";
    public int lastgenNetPayId=0;
    public String[] dbtables = {
        "tbl_client",
        "tbl_client_has_tbl_project",
        "tbl_deduction",
        "tbl_emp_prof",
        "tbl_emp_valid_ids",
        "tbl_emp_valid_ids_has_tbl_emp_prof",
        "tbl_netpay",
        "tbl_overtime",
        "tbl_position",
        "tbl_project",
        "tbl_netpay_has_tbl_deduction"
    };
    public ArrayList positionAl = new ArrayList();
    public Payslip ps;
    
    
    /**
     * Creates new form mainwindow
     */
    public void timeStamp(){
        java.util.Date date = new java.util.Date();
        java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
        
    }
    public void dbHandler(){
//        this.jComboBox2.addItem("vghgg","ghghgh");
        try {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
//                  /--------------------
//            Statement st = this.conn.createStatement();
//            ResultSet res = st.executeQuery("SELECT * FROM tbl_emp_prof"); 
//                  while (res.next()) { 
//                    String id = res.getString("emp_id");
//                    String msg = res.getString("emp_fname");
//                    System.out.println(id + "\t" + msg);
//            }
            long timestamp = System.currentTimeMillis()/1000;
            String timestamp2 = Long.toString(timestamp);
            String sql = "INSERT INTO tbl_emp_prof (emp_id,emp_lname, emp_fname, emp_bday, emp_address, emp_standing) VALUES (?,?, ?, ?, ?,?)";
            PreparedStatement statement = this.conn.prepareStatement(sql);
            statement.setString(1, "testid");
            statement.setString(2, "bill");
            statement.setString(3, "secretpass");
            statement.setString(4, timestamp2);
            statement.setString(5, "bill.gates@microsoft.com");
            statement.setString(6, "firedagain");
            
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
            }
            
            this.conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
   
    private String getTimestamp() {
//        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//       
        try{
            //From Jdatechooser to date format to 
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String date = sdf.format(this.emp_bday_calendar.getDate());
            Date date2 = sdf.parse(date);
            //to timestamp
            long millis = date2.getTime();
            
            //from timestamp to date to dateformat
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date date3=new Date(millis);
            String date4 = sdf2.format(date3);
            
            
            System.out.println(millis);
            System.out.println(date3);
            System.out.println(date4);
            
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            return "**********************";
        }
        
//        return dateFormat.format(date);
    }  
    public String todaysDate(){
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
        String date = sdf.format(today);
        return date;
    }
    public mainwindow() {
//       this.jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5" }));
//         this.jComboBox2.addItem("ddd");
//        long epoch = System.currentTimeMillis()/1000;
//        System.out.println("Timestamp!");
//        System.out.println(epoch);
//         dbHandler();
        ZoneId zone2 = ZoneId.of("Asia/Manila");

        initComponents();
        this.setLocationRelativeTo(null);
        this.overtime_panel.setVisible(false);
        this.deductions_panel.setVisible(false);
        this.client_panel.setVisible(false);
        this.payslip_panel.setVisible(true);
        this.employee_panel.setVisible(false);
        this.otherid_panel.setVisible(false);
        this.paySlipcomboOvertime();
        this.paySlipcomboPosition();
        this.paySlipcomboClient();
        this.paySlipcomboProject();
//        this.paySlipcomboDeductions();
//        this.paySlipJlistDeductions();
        this.pdfwriter();
//        this.tempPayslipDeductions[0][0]=null;
//        this.tempPayslipDeductions[0][1]=null;
//                  this.tempPayslipDeductions[i][1]=tempb;
        this.modelPayslipDeductionsfunc();
//        this.dummyInitializer();
//        Arrays.toString(this.tempPayslipDeductions);
        this.payslipDeductionTblSelected = "";
//        pdfwriter2.main(dbtables);
//         System.out.println("today "+this.todaysDate());

//        System.out.println(pdf.variablenaem+"---------------------------------");
         
    }
    public void modelPayslipDeductionsfunc(){
        DefaultTableModel dm = (DefaultTableModel)this.paylist_deduction_tbl.getModel();
//        model.addRow(new Object[]{"fdfs",1});
//        model.addRow(new Object[]{"fdfs",1});
//        model.addRow(new Object[]{"fdfs",1});
        int rowCount = dm.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            dm.removeRow(i);
        }
        try {  
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            /**
             * Start
             */
            String sql2 = "SELECT count(*) FROM "+this.dbtables[2];
            PreparedStatement st2 = this.conn.prepareStatement(sql2);
//            st.setString(1, searchid);
            ResultSet res2 = st2.executeQuery(); 
            if(res2.next()){
                int numRows = res2.getInt(1);
//                this.tempList_other_id= new String[numRows];
                System.out.println("Rows:"+numRows);
                this.tempPayslipDeductions=new Object[numRows][2];
//                System.out.println(numRows+" ***********");
                
            }
            
            String sql = "SELECT * FROM "+this.dbtables[2];
            PreparedStatement st = this.conn.prepareStatement(sql);
//            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
//            this.payslip_combox_deductions1.removeAllItems();
            int i = 0;
            while (res.next()) { 
//                this.payslip_combox_deductions1.addItem(res.getString("ded_name")+"-"+res.getString("ded_value"));   
                int id = Integer.parseInt(res.getString("ded_id"));
                String tempa =res.getString("ded_name");
                int tempb = Integer.parseInt(res.getString("ded_value"));
                dm.addRow(new Object[]{id,tempa,tempb});
//                for(int a=0;a<=2;a++){
                  System.out.println(i);
                  this.tempPayslipDeductions[i][0]=id;
                  this.tempPayslipDeductions[i][1]=tempb;
//                }
                
                i++;
            }
            /**
             * End of overtime
             */
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    public void pdfwriter(){
            
//            addTitlePage(document);
//            addContent(document);
//            document.close();
    }
    public void menuHover(int menuId){
        Color darkred = new Color(102,0,0);
        Color darkblue = new Color(0,0,77);
        this.hoverPanel_menu1_payslip.setBackground(darkblue);
        this.hoverPanel_menu_employee.setBackground(darkblue);
        this.hoverPanel_menu_clinet_project.setBackground(darkblue);
        this.hoverPanel_menu_deductions.setBackground(darkblue);
        this.hoverPanel_menu_overtime.setBackground(darkblue);
        this.hoverPanel_menu_other_id.setBackground(darkblue);
        if(menuId==1){
            this.hoverPanel_menu1_payslip.setBackground(darkred);
        }
        if(menuId==2){
            this.hoverPanel_menu_employee.setBackground(darkred);
        }
        if(menuId==3){
            this.hoverPanel_menu_clinet_project.setBackground(darkred);
        }
        if(menuId==4){
            this.hoverPanel_menu_deductions.setBackground(darkred);
        }
        if(menuId==5){
            this.hoverPanel_menu_overtime.setBackground(darkred);
        }
        if(menuId==6){
            this.hoverPanel_menu_other_id.setBackground(darkred);
        }

    }
    public void paySlipcomboOvertime(){
       
        try {  
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            /**
             * Start of overtime
             */
            String sql = "SELECT * FROM "+this.dbtables[7];
            PreparedStatement st = this.conn.prepareStatement(sql);
//            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            this.payslip_combox_overtime1.removeAllItems();
            while (res.next()) { 
                this.payslip_combox_overtime1.addItem(res.getString("ovr_time_id")+","+res.getString("ovr_time_pay"));          
            }
            /**
             * End of overtime
             */
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    public void paySlipcomboPosition(){
       
        try {  
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            /**
             * Start of overtime
             */
            String sql = "SELECT * FROM "+this.dbtables[8];
            PreparedStatement st = this.conn.prepareStatement(sql);
//            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            this.payslip_combox_position1.removeAllItems();
//            String[][] tempx = new String[0][3];
//            StringTokenizer st6 = new StringTokenizer(",");
//            ArrayList al = new ArrayList();
//            int i = 0;
            while (res.next()) { 
                this.payslip_combox_position1.addItem(res.getString("pos_id")+","+res.getString("pos_name")+","+res.getString("pos_salary"));    
//                this.positionAl.add(res.getString("pos_id")+"-"+res.getString("pos_name")+"-"+res.getString("pos_salary"));
//                i++;
            }
//            al.get
//            this.positionData = new String[i][3];
//            System.out.println("*****************");
//            System.out.println(al);
            /**
             * End of overtime
             */
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    public void paySlipcomboClient(){
       
        try {  
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            /**
             * Start of overtime
             */
            String sql = "SELECT * FROM "+this.dbtables[0];
            PreparedStatement st = this.conn.prepareStatement(sql);
//            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            this.payslip_combox_client1.removeAllItems();
            this.add_proj_combolist.removeAllItems();
            while (res.next()) { 
                this.payslip_combox_client1.addItem(res.getString("client_id")+", "+res.getString("client_name"));   
                this.add_proj_combolist.addItem(res.getString("client_id")+", "+res.getString("client_name"));   
            }
            /**
             * End of overtime
             */
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    public void paySlipcomboProject(){
       
        try {  
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            /**
             * Start of overtime
             */
            String sql = "SELECT * FROM "+this.dbtables[9];
            PreparedStatement st = this.conn.prepareStatement(sql);
//            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            this.add_proj_combolist1.removeAllItems();
            this.payslip_combox_project1.removeAllItems();
//            this.add_proj_combolist.removeAllItems();
            while (res.next()) { 
                this.payslip_combox_project1.addItem(res.getString("prj_id")+", "+res.getString("prj_name"));   
                this.add_proj_combolist1.addItem(res.getString("prj_id")+", "+res.getString("prj_name"));   
            }
            /**
             * End of overtime
             */
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
//    public void paySlipcomboDeductions(){ // candidate for removal
//       
//        try {  
//            Class.forName(driver).newInstance();
//            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
////                  /--------------------
//            /**
//             * Start of overtime
//             */
//            String sql = "SELECT * FROM "+this.dbtables[2];
//            PreparedStatement st = this.conn.prepareStatement(sql);
////            st.setString(1, searchid);
//            ResultSet res = st.executeQuery(); 
//            this.payslip_combox_deductions1.removeAllItems();
//            while (res.next()) { 
//                this.payslip_combox_deductions1.addItem(res.getString("ded_name")+"-"+res.getString("ded_value"));          
//            }
//            /**
//             * End of overtime
//             */
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        finally{
//            try{
//                if(this.conn!=null){
//                    this.conn.close();
//                }
//            }catch(Exception e){
//                e.printStackTrace();
//            }
//        }
//    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jDialog2 = new javax.swing.JDialog();
        jLabel61 = new javax.swing.JLabel();
        jPanel23 = new javax.swing.JPanel();
        jLabel63 = new javax.swing.JLabel();
        valid_id_name_fld3 = new javax.swing.JTextField();
        jSeparator28 = new javax.swing.JSeparator();
        other_id_add_btn2 = new javax.swing.JButton();
        jLabel64 = new javax.swing.JLabel();
        jPanel25 = new javax.swing.JPanel();
        add_other_id_JList1 = new javax.swing.JScrollPane();
        valid_other_id_list1 = new javax.swing.JList<>();
        add_other_id_delete1 = new javax.swing.JButton();
        jLabel66 = new javax.swing.JLabel();
        header_panel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        body_main_panel = new javax.swing.JPanel();
        payslip_panel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        generate_payslip_menu_btn = new javax.swing.JButton();
        payslip_combox_project1 = new javax.swing.JComboBox<>();
        jLabel30 = new javax.swing.JLabel();
        payslip_combox_client1 = new javax.swing.JComboBox<>();
        payslip_combox_position1 = new javax.swing.JComboBox<>();
        jLabel31 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        payslip_combox_overtime1 = new javax.swing.JComboBox<>();
        payslip_fullname_lbl1 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        gen_payslip_emp_id_fld = new javax.swing.JTextField();
        jSeparator12 = new javax.swing.JSeparator();
        jLabel56 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        gen_payslip_emp_hoursworked = new javax.swing.JTextField();
        jSeparator22 = new javax.swing.JSeparator();
        jLabel57 = new javax.swing.JLabel();
        payslip_clear_deductions_fld = new javax.swing.JLabel();
        payslip_ded_fld = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        paylist_deduction_tbl = new javax.swing.JTable();
        jLabel52 = new javax.swing.JLabel();
        gen_payslip_emp_ot_thours = new javax.swing.JTextField();
        jSeparator25 = new javax.swing.JSeparator();
        jLabel53 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel73 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        payslip_netpay_res_fld = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        payslip_grosspay_res_fld = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        payslip_deductions_res_fld = new javax.swing.JLabel();
        employee_panel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel7 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        emp_id_fld = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel19 = new javax.swing.JLabel();
        emp_fname_fld = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        emp_lname_fld = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel22 = new javax.swing.JLabel();
        emp_add_fld = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        jLabel23 = new javax.swing.JLabel();
        emp_standing_fld = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        emp_bday_calendar = new com.toedter.calendar.JDateChooser();
        jLabel32 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jSeparator19 = new javax.swing.JSeparator();
        jPanel13 = new javax.swing.JPanel();
        jSeparator8 = new javax.swing.JSeparator();
        emp_update_calendar = new com.toedter.calendar.JDateChooser();
        delete_click = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        emp_update_address_fld = new javax.swing.JTextField();
        jSeparator23 = new javax.swing.JSeparator();
        jLabel27 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        update_client = new javax.swing.JButton();
        emp_update_lname_fld = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        emp_update_standing_fld = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        emp_update_emp_id_fld = new javax.swing.JTextField();
        jSeparator10 = new javax.swing.JSeparator();
        jSeparator11 = new javax.swing.JSeparator();
        emp_update_fname_fld = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        username18 = new javax.swing.JTextField();
        jComboBox13 = new javax.swing.JComboBox<>();
        jLabel43 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        employee_rec_link_empid_cmb = new javax.swing.JComboBox<>();
        jLabel44 = new javax.swing.JLabel();
        jSeparator20 = new javax.swing.JSeparator();
        jLabel48 = new javax.swing.JLabel();
        deductions_panel = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        delete_ded_btn = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        add_deduction_jlist = new javax.swing.JList<>();
        jPanel17 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        ded_name_fld1 = new javax.swing.JTextField();
        jSeparator16 = new javax.swing.JSeparator();
        ded_value_fld1 = new javax.swing.JTextField();
        jSeparator21 = new javax.swing.JSeparator();
        jLabel42 = new javax.swing.JLabel();
        add_ded1 = new javax.swing.JButton();
        overtime_panel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jPanel16 = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        ovr_time_rates_fld = new javax.swing.JTextField();
        jSeparator14 = new javax.swing.JSeparator();
        ovr_add_btn = new javax.swing.JButton();
        jPanel18 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        add_overtime_jlist = new javax.swing.JList<>();
        over_del_but = new javax.swing.JButton();
        client_panel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        proj_name_fld = new javax.swing.JTextField();
        jSeparator15 = new javax.swing.JSeparator();
        jLabel34 = new javax.swing.JLabel();
        add_proj_btn = new javax.swing.JButton();
        add_proj_end_jchooser = new com.toedter.calendar.JDateChooser();
        jLabel37 = new javax.swing.JLabel();
        add_proj_start_jchooser = new com.toedter.calendar.JDateChooser();
        jLabel40 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jSeparator18 = new javax.swing.JSeparator();
        add_client_btn = new javax.swing.JButton();
        proj_client_address_fld1 = new javax.swing.JTextField();
        proj_client_name_fld1 = new javax.swing.JTextField();
        jSeparator26 = new javax.swing.JSeparator();
        jPanel22 = new javax.swing.JPanel();
        add_proj_combolist = new javax.swing.JComboBox<>();
        jLabel55 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        add_proj_combolist1 = new javax.swing.JComboBox<>();
        add_client_btn2 = new javax.swing.JButton();
        jLabel62 = new javax.swing.JLabel();
        otherid_panel = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jLabel72 = new javax.swing.JLabel();
        add_other_id_JList2 = new javax.swing.JScrollPane();
        emp_position_jlist = new javax.swing.JList<>();
        emp_pos_delete = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        jLabel49 = new javax.swing.JLabel();
        valid_id_name_fld2 = new javax.swing.JTextField();
        jSeparator24 = new javax.swing.JSeparator();
        other_id_add_btn1 = new javax.swing.JButton();
        jLabel67 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jPanel24 = new javax.swing.JPanel();
        jLabel65 = new javax.swing.JLabel();
        employee_position_name_fld = new javax.swing.JTextField();
        jSeparator29 = new javax.swing.JSeparator();
        employee_position_btn = new javax.swing.JButton();
        jSeparator30 = new javax.swing.JSeparator();
        employee_position_rate_fld = new javax.swing.JTextField();
        jLabel69 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        jPanel26 = new javax.swing.JPanel();
        jLabel71 = new javax.swing.JLabel();
        add_other_id_JList = new javax.swing.JScrollPane();
        valid_other_id_list = new javax.swing.JList<>();
        add_other_id_delete = new javax.swing.JButton();
        menu = new javax.swing.JPanel();
        add_empoyee_btn2 = new javax.swing.JLabel();
        hoverPanel_menu1_payslip = new javax.swing.JPanel();
        generate_payslip_btn = new javax.swing.JLabel();
        hoverPanel_menu_employee = new javax.swing.JPanel();
        add_empoyee_btn = new javax.swing.JLabel();
        hoverPanel_menu_clinet_project = new javax.swing.JPanel();
        add_client_btn1 = new javax.swing.JLabel();
        hoverPanel_menu_overtime = new javax.swing.JPanel();
        add_overtime_rate = new javax.swing.JLabel();
        hoverPanel_menu_other_id = new javax.swing.JPanel();
        add_otherid_btn = new javax.swing.JLabel();
        hoverPanel_menu_deductions = new javax.swing.JPanel();
        add_deductions_btn = new javax.swing.JLabel();
        hoverPanel_menu_other_id1 = new javax.swing.JPanel();
        add_otherid_btn1 = new javax.swing.JLabel();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        jLabel61.setForeground(new java.awt.Color(0, 0, 102));
        jLabel61.setText("Project ID: ");

        jPanel23.setBackground(new java.awt.Color(204, 204, 204));
        jPanel23.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel63.setForeground(new java.awt.Color(0, 0, 102));
        jLabel63.setText("Valid ID Name:");
        jPanel23.add(jLabel63, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        valid_id_name_fld3.setBackground(new java.awt.Color(204, 204, 204));
        valid_id_name_fld3.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        valid_id_name_fld3.setForeground(new java.awt.Color(0, 0, 102));
        valid_id_name_fld3.setText("xxxxxxxxxxxxxxxxxx");
        valid_id_name_fld3.setBorder(null);
        valid_id_name_fld3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                valid_id_name_fld3MouseClicked(evt);
            }
        });
        jPanel23.add(valid_id_name_fld3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, 170, -1));

        jSeparator28.setBackground(new java.awt.Color(255, 255, 255));
        jPanel23.add(jSeparator28, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 40, 130, 10));

        other_id_add_btn2.setBackground(new java.awt.Color(0, 0, 51));
        other_id_add_btn2.setForeground(new java.awt.Color(255, 255, 255));
        other_id_add_btn2.setText("Add ID");
        other_id_add_btn2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                other_id_add_btn2MouseClicked(evt);
            }
        });
        other_id_add_btn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                other_id_add_btn2ActionPerformed(evt);
            }
        });
        jPanel23.add(other_id_add_btn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, 130, -1));

        jLabel64.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        jLabel64.setForeground(new java.awt.Color(0, 0, 102));
        jLabel64.setText("Example: SSS, Pagibig...");
        jPanel23.add(jLabel64, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 220, 30));

        jPanel25.setBackground(new java.awt.Color(204, 204, 204));
        jPanel25.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        valid_other_id_list1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        valid_other_id_list1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        add_other_id_JList1.setViewportView(valid_other_id_list1);

        jPanel25.add(add_other_id_JList1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 290, 100));

        add_other_id_delete1.setBackground(new java.awt.Color(0, 0, 51));
        add_other_id_delete1.setForeground(new java.awt.Color(255, 255, 255));
        add_other_id_delete1.setText("Delete");
        add_other_id_delete1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_other_id_delete1MouseClicked(evt);
            }
        });
        jPanel25.add(add_other_id_delete1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 140, 130, -1));

        jLabel66.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel66.setForeground(new java.awt.Color(0, 0, 102));
        jLabel66.setText("Other ID");
        jPanel25.add(jLabel66, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 220, 30));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage(mainwindow.class.getResource("128x128.png"))
        );
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        header_panel.setBackground(new java.awt.Color(196, 0, 1));
        header_panel.setName(""); // NOI18N
        header_panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setBackground(new java.awt.Color(196, 0, 1));
        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\maestrom4\\Documents\\NetBeansProjects\\payroll_sam_angel\\src\\payroll\\img\\header5.jpg")); // NOI18N
        header_panel.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 2, 770, 160));

        getContentPane().add(header_panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, 780, 162));
        header_panel.getAccessibleContext().setAccessibleName("");

        body_main_panel.setBackground(new java.awt.Color(51, 51, 51));
        body_main_panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        payslip_panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(0, 0, 153));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Futura LT", 0, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Generate Payslip");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 15, -1, -1));

        payslip_panel.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 780, 60));
        payslip_panel.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 170, -1, -1));

        jPanel21.setBackground(new java.awt.Color(204, 204, 204));
        jPanel21.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        generate_payslip_menu_btn.setBackground(new java.awt.Color(0, 0, 51));
        generate_payslip_menu_btn.setForeground(new java.awt.Color(255, 255, 255));
        generate_payslip_menu_btn.setText("Generate");
        generate_payslip_menu_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                generate_payslip_menu_btnMouseClicked(evt);
            }
        });
        jPanel21.add(generate_payslip_menu_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 150, 150, -1));

        payslip_combox_project1.setForeground(new java.awt.Color(0, 0, 102));
        payslip_combox_project1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tilling", "Repaint", "Flooring", "Walling" }));
        jPanel21.add(payslip_combox_project1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 120, 150, -1));

        jLabel30.setForeground(new java.awt.Color(0, 0, 102));
        jLabel30.setText("Client: ");
        jPanel21.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 100, 20));

        payslip_combox_client1.setForeground(new java.awt.Color(0, 0, 102));
        payslip_combox_client1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Perma", "Gaisano", "Ketkai", "SM", "Malls" }));
        jPanel21.add(payslip_combox_client1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 90, 150, -1));

        payslip_combox_position1.setForeground(new java.awt.Color(0, 0, 102));
        payslip_combox_position1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Utility", "Mason", "Carpenter", "Tiler", "Electrician", "Engr.", "Accountant" }));
        jPanel21.add(payslip_combox_position1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 110, 150, -1));

        jLabel31.setForeground(new java.awt.Color(0, 0, 102));
        jLabel31.setText("Position/Rate:");
        jPanel21.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 110, 100, 20));

        jLabel46.setForeground(new java.awt.Color(0, 0, 102));
        jLabel46.setText("Overtime rate:");
        jPanel21.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 80, 100, 20));

        payslip_combox_overtime1.setForeground(new java.awt.Color(0, 0, 102));
        payslip_combox_overtime1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "100", "200", "250", "300", "350", "400", "450", "500", "550" }));
        jPanel21.add(payslip_combox_overtime1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 80, 150, -1));

        payslip_fullname_lbl1.setForeground(new java.awt.Color(0, 0, 102));
        payslip_fullname_lbl1.setText("Testname TestLname");
        jPanel21.add(payslip_fullname_lbl1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 140, -1));

        jLabel12.setForeground(new java.awt.Color(0, 0, 102));
        jLabel12.setText("Full Name:");
        jPanel21.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 100, 20));

        jLabel47.setForeground(new java.awt.Color(0, 0, 102));
        jLabel47.setIcon(new javax.swing.ImageIcon("C:\\Users\\maestrom4\\Documents\\NetBeansProjects\\payroll_sam_angel\\src\\payroll\\img\\search_icon.png")); // NOI18N
        jPanel21.add(jLabel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 15, 30, 30));

        gen_payslip_emp_id_fld.setBackground(new java.awt.Color(204, 204, 204));
        gen_payslip_emp_id_fld.setForeground(new java.awt.Color(0, 0, 102));
        gen_payslip_emp_id_fld.setBorder(null);
        gen_payslip_emp_id_fld.setDragEnabled(true);
        gen_payslip_emp_id_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                gen_payslip_emp_id_fldMouseClicked(evt);
            }
        });
        gen_payslip_emp_id_fld.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                gen_payslip_emp_id_fldKeyPressed(evt);
            }
        });
        jPanel21.add(gen_payslip_emp_id_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 20, 130, -1));

        jSeparator12.setBackground(new java.awt.Color(0, 0, 51));
        jPanel21.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 130, -1));

        jLabel56.setFont(new java.awt.Font("Dialog", 2, 10)); // NOI18N
        jLabel56.setForeground(new java.awt.Color(0, 0, 102));
        jLabel56.setText("Note: Click to add deductions");
        jPanel21.add(jLabel56, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 320, 150, 20));

        jLabel51.setForeground(new java.awt.Color(0, 0, 102));
        jLabel51.setText("Hours worked:");
        jPanel21.add(jLabel51, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 20, 100, 20));

        gen_payslip_emp_hoursworked.setBackground(new java.awt.Color(204, 204, 204));
        gen_payslip_emp_hoursworked.setForeground(new java.awt.Color(0, 0, 102));
        gen_payslip_emp_hoursworked.setBorder(null);
        gen_payslip_emp_hoursworked.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                gen_payslip_emp_hoursworkedMouseClicked(evt);
            }
        });
        gen_payslip_emp_hoursworked.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                gen_payslip_emp_hoursworkedKeyPressed(evt);
            }
        });
        jPanel21.add(gen_payslip_emp_hoursworked, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 20, 140, -1));

        jSeparator22.setBackground(new java.awt.Color(0, 0, 51));
        jPanel21.add(jSeparator22, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 40, 140, -1));

        jLabel57.setForeground(new java.awt.Color(0, 0, 102));
        jLabel57.setText("Deductions:");
        jPanel21.add(jLabel57, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 70, 20));

        payslip_clear_deductions_fld.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        payslip_clear_deductions_fld.setForeground(new java.awt.Color(0, 0, 102));
        payslip_clear_deductions_fld.setText("clear");
        payslip_clear_deductions_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                payslip_clear_deductions_fldMouseClicked(evt);
            }
        });
        jPanel21.add(payslip_clear_deductions_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 210, 30, 20));
        jPanel21.add(payslip_ded_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 160, 150, 50));

        paylist_deduction_tbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Name", "Value"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        paylist_deduction_tbl.setCellSelectionEnabled(true);
        paylist_deduction_tbl.setDragEnabled(true);
        paylist_deduction_tbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                paylist_deduction_tblMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(paylist_deduction_tbl);

        jPanel21.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 230, 70));

        jLabel52.setForeground(new java.awt.Color(0, 0, 102));
        jLabel52.setText("Employee ID: ");
        jPanel21.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        gen_payslip_emp_ot_thours.setBackground(new java.awt.Color(204, 204, 204));
        gen_payslip_emp_ot_thours.setForeground(new java.awt.Color(0, 0, 102));
        gen_payslip_emp_ot_thours.setBorder(null);
        gen_payslip_emp_ot_thours.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                gen_payslip_emp_ot_thoursMouseClicked(evt);
            }
        });
        gen_payslip_emp_ot_thours.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                gen_payslip_emp_ot_thoursKeyPressed(evt);
            }
        });
        jPanel21.add(gen_payslip_emp_ot_thours, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 50, 140, -1));

        jSeparator25.setBackground(new java.awt.Color(0, 0, 51));
        jPanel21.add(jSeparator25, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 70, 140, -1));

        jLabel53.setForeground(new java.awt.Color(0, 0, 102));
        jLabel53.setText("OT hours:");
        jPanel21.add(jLabel53, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 50, 80, 20));

        jLabel10.setForeground(new java.awt.Color(0, 0, 102));
        jLabel10.setText("Project: ");
        jPanel21.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 70, 20));

        jLabel13.setForeground(new java.awt.Color(0, 0, 102));
        jLabel13.setText("Project: ");
        jPanel21.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 70, 20));

        jLabel59.setForeground(new java.awt.Color(0, 0, 102));
        jLabel59.setText("Deductions");
        jPanel21.add(jLabel59, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 70, 20));

        jLabel73.setForeground(new java.awt.Color(0, 0, 102));
        jLabel73.setText("List:");
        jPanel21.add(jLabel73, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 220, 70, 20));

        payslip_panel.add(jPanel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 540, 340));

        jPanel11.setBackground(new java.awt.Color(204, 204, 204));
        jPanel11.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 102));
        jLabel7.setText("Net Pay: ");
        jPanel11.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 70, 20));

        payslip_netpay_res_fld.setForeground(new java.awt.Color(0, 0, 102));
        payslip_netpay_res_fld.setText("0.0000000");
        jPanel11.add(payslip_netpay_res_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 70, 20));

        jLabel15.setForeground(new java.awt.Color(0, 0, 102));
        jLabel15.setText("Gross Pay: ");
        jPanel11.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 20));

        payslip_grosspay_res_fld.setForeground(new java.awt.Color(0, 0, 102));
        payslip_grosspay_res_fld.setText("0.0000000");
        jPanel11.add(payslip_grosspay_res_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, 70, 20));

        jLabel54.setForeground(new java.awt.Color(0, 0, 102));
        jLabel54.setText("Total Deductions:");
        jPanel11.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 100, 20));

        payslip_deductions_res_fld.setForeground(new java.awt.Color(0, 0, 102));
        payslip_deductions_res_fld.setText("0.0000000");
        jPanel11.add(payslip_deductions_res_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 80, 70, 20));

        payslip_panel.add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 90, 190, 160));

        body_main_panel.add(payslip_panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 10, 770, 445));

        employee_panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(0, 0, 153));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("Futura LT", 0, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Employee Record");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 15, -1, -1));

        employee_panel.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 850, 60));

        jPanel7.setBackground(new java.awt.Color(214, 217, 223));
        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel12.setBackground(new java.awt.Color(204, 204, 204));
        jPanel12.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel18.setForeground(new java.awt.Color(0, 0, 102));
        jLabel18.setText("Employee ID: ");
        jPanel12.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        emp_id_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_id_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_id_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_id_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_id_fld.setBorder(null);
        emp_id_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                emp_id_fldMouseClicked(evt);
            }
        });
        emp_id_fld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emp_id_fldActionPerformed(evt);
            }
        });
        jPanel12.add(emp_id_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 30, 170, -1));

        jSeparator2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 50, 170, 10));

        jLabel19.setForeground(new java.awt.Color(0, 0, 102));
        jLabel19.setText("First Name:");
        jPanel12.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));

        emp_fname_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_fname_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_fname_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_fname_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_fname_fld.setBorder(null);
        emp_fname_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                emp_fname_fldMouseClicked(evt);
            }
        });
        jPanel12.add(emp_fname_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 70, 170, -1));

        jSeparator3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 170, 10));

        jSeparator4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 90, 170, 10));

        emp_lname_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_lname_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_lname_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_lname_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_lname_fld.setBorder(null);
        emp_lname_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                emp_lname_fldMouseClicked(evt);
            }
        });
        emp_lname_fld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emp_lname_fldActionPerformed(evt);
            }
        });
        jPanel12.add(emp_lname_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 100, 170, -1));

        jLabel20.setForeground(new java.awt.Color(0, 0, 102));
        jLabel20.setText("Last Name:");
        jPanel12.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, -1, -1));

        jSeparator6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 200, 170, 10));

        jLabel22.setForeground(new java.awt.Color(0, 0, 102));
        jLabel22.setText("Address:");
        jPanel12.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, -1, -1));

        emp_add_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_add_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_add_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_add_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_add_fld.setBorder(null);
        emp_add_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                emp_add_fldMouseClicked(evt);
            }
        });
        jPanel12.add(emp_add_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 180, 170, -1));

        jSeparator7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 240, 170, 10));

        jLabel23.setForeground(new java.awt.Color(0, 0, 102));
        jLabel23.setText("Standing:");
        jPanel12.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, -1, -1));

        emp_standing_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_standing_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_standing_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_standing_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_standing_fld.setBorder(null);
        emp_standing_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                emp_standing_fldMouseClicked(evt);
            }
        });
        emp_standing_fld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emp_standing_fldActionPerformed(evt);
            }
        });
        jPanel12.add(emp_standing_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 220, 170, -1));

        jButton3.setBackground(new java.awt.Color(0, 0, 51));
        jButton3.setForeground(new java.awt.Color(255, 255, 255));
        jButton3.setText("Add ");
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
        });
        jPanel12.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 250, 180, -1));

        emp_bday_calendar.setDateFormatString("MM/d/yyyy");
        emp_bday_calendar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                emp_bday_calendarMouseClicked(evt);
            }
        });
        jPanel12.add(emp_bday_calendar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 170, -1));

        jLabel32.setForeground(new java.awt.Color(0, 0, 102));
        jLabel32.setText("Birthday:");
        jPanel12.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, -1, -1));

        jPanel7.add(jPanel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 360, 290));

        jTabbedPane1.addTab("Add Employee", jPanel7);

        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jSeparator19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.add(jSeparator19, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 160, 10));

        jPanel13.setBackground(new java.awt.Color(204, 204, 204));
        jPanel13.setForeground(new java.awt.Color(0, 0, 102));
        jPanel13.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jSeparator8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 170, 10));

        emp_update_calendar.setDateFormatString("MM/d/yyyy");
        emp_update_calendar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                emp_update_calendarFocusGained(evt);
            }
        });
        jPanel13.add(emp_update_calendar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 140, 170, -1));

        delete_click.setBackground(new java.awt.Color(0, 0, 51));
        delete_click.setForeground(new java.awt.Color(255, 255, 255));
        delete_click.setText("Delete");
        delete_click.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                delete_clickMouseClicked(evt);
            }
        });
        jPanel13.add(delete_click, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 60, 120, -1));

        jLabel26.setForeground(new java.awt.Color(0, 0, 102));
        jLabel26.setText("Last Name:");
        jPanel13.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));

        emp_update_address_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_update_address_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_update_address_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_update_address_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_update_address_fld.setBorder(null);
        emp_update_address_fld.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                emp_update_address_fldFocusGained(evt);
            }
        });
        jPanel13.add(emp_update_address_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 30, 170, -1));

        jSeparator23.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.add(jSeparator23, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 170, 10));

        jLabel27.setForeground(new java.awt.Color(0, 0, 102));
        jLabel27.setText("Birthday:");
        jPanel13.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));

        jLabel25.setForeground(new java.awt.Color(0, 0, 102));
        jLabel25.setText("First Name:");
        jPanel13.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));

        update_client.setBackground(new java.awt.Color(0, 0, 51));
        update_client.setForeground(new java.awt.Color(255, 255, 255));
        update_client.setText("Update");
        update_client.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                update_clientMouseClicked(evt);
            }
        });
        jPanel13.add(update_client, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 20, 120, -1));

        emp_update_lname_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_update_lname_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_update_lname_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_update_lname_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_update_lname_fld.setBorder(null);
        emp_update_lname_fld.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                emp_update_lname_fldFocusGained(evt);
            }
        });
        jPanel13.add(emp_update_lname_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 100, 170, -1));

        jLabel28.setForeground(new java.awt.Color(0, 0, 102));
        jLabel28.setText("Address:");
        jPanel13.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 30, -1, -1));

        emp_update_standing_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_update_standing_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_update_standing_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_update_standing_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_update_standing_fld.setBorder(null);
        emp_update_standing_fld.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                emp_update_standing_fldFocusGained(evt);
            }
        });
        jPanel13.add(emp_update_standing_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 70, 170, -1));

        jLabel29.setForeground(new java.awt.Color(0, 0, 102));
        jLabel29.setText("Standing:");
        jPanel13.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 70, -1, -1));

        jLabel24.setForeground(new java.awt.Color(0, 0, 102));
        jLabel24.setText("Employee ID: ");
        jPanel13.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        emp_update_emp_id_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_update_emp_id_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_update_emp_id_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_update_emp_id_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_update_emp_id_fld.setBorder(null);
        emp_update_emp_id_fld.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                emp_update_emp_id_fldFocusGained(evt);
            }
        });
        emp_update_emp_id_fld.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                emp_update_emp_id_fldKeyPressed(evt);
            }
        });
        jPanel13.add(emp_update_emp_id_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 30, 170, -1));

        jSeparator10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 50, 170, 10));

        jSeparator11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 90, 170, 10));

        emp_update_fname_fld.setBackground(new java.awt.Color(204, 204, 204));
        emp_update_fname_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        emp_update_fname_fld.setForeground(new java.awt.Color(0, 0, 102));
        emp_update_fname_fld.setText("xxxxxxxxxxxxxxxxxx");
        emp_update_fname_fld.setBorder(null);
        emp_update_fname_fld.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                emp_update_fname_fldFocusGained(evt);
            }
        });
        emp_update_fname_fld.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                emp_update_fname_fldKeyPressed(evt);
            }
        });
        jPanel13.add(emp_update_fname_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, 170, -1));

        jPanel8.add(jPanel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 730, 180));

        jPanel14.setBackground(new java.awt.Color(204, 204, 204));
        jPanel14.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton4.setBackground(new java.awt.Color(0, 0, 51));
        jButton4.setForeground(new java.awt.Color(255, 255, 255));
        jButton4.setText("Link");
        jPanel14.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 30, 120, -1));

        username18.setBackground(new java.awt.Color(204, 204, 204));
        username18.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        username18.setForeground(new java.awt.Color(0, 0, 102));
        username18.setText("xxxxxxxxxxxxxxxxxx");
        username18.setBorder(null);
        jPanel14.add(username18, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 40, 170, 20));

        jComboBox13.setEditable(true);
        jComboBox13.setForeground(new java.awt.Color(0, 0, 102));
        jComboBox13.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel14.add(jComboBox13, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, 140, -1));

        jLabel43.setForeground(new java.awt.Color(0, 0, 102));
        jLabel43.setText("ID type number:");
        jPanel14.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 40, -1, -1));

        jLabel33.setForeground(new java.awt.Color(0, 0, 102));
        jLabel33.setText("Link Other Contribution ID(s)");
        jPanel14.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        employee_rec_link_empid_cmb.setEditable(true);
        employee_rec_link_empid_cmb.setForeground(new java.awt.Color(0, 0, 102));
        employee_rec_link_empid_cmb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel14.add(employee_rec_link_empid_cmb, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 140, -1));

        jLabel44.setForeground(new java.awt.Color(0, 0, 102));
        jLabel44.setText("Employee ID: ");
        jPanel14.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        jSeparator20.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.add(jSeparator20, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 60, 170, 10));

        jLabel48.setForeground(new java.awt.Color(0, 0, 102));
        jLabel48.setText("ID type:");
        jPanel14.add(jLabel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));

        jPanel8.add(jPanel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 200, 730, 100));

        jTabbedPane1.addTab("Update Employee", jPanel8);

        employee_panel.add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 750, 330));

        body_main_panel.add(employee_panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 10, 770, 445));
        employee_panel.getAccessibleContext().setAccessibleName("");

        deductions_panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(0, 0, 153));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel8.setFont(new java.awt.Font("Futura LT", 0, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Add Deductions");
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 15, -1, -1));

        deductions_panel.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 850, 60));

        jPanel15.setBackground(new java.awt.Color(204, 204, 204));
        jPanel15.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        delete_ded_btn.setBackground(new java.awt.Color(0, 0, 51));
        delete_ded_btn.setForeground(new java.awt.Color(255, 255, 255));
        delete_ded_btn.setText("Delete");
        delete_ded_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                delete_ded_btnMouseClicked(evt);
            }
        });
        jPanel15.add(delete_ded_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 140, 150, -1));

        add_deduction_jlist.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(add_deduction_jlist);

        jPanel15.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 320, 120));

        deductions_panel.add(jPanel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 240, 370, 180));

        jPanel17.setBackground(new java.awt.Color(204, 204, 204));
        jPanel17.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel41.setForeground(new java.awt.Color(0, 0, 102));
        jLabel41.setText("Deduction Name:");
        jPanel17.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, -1, -1));

        ded_name_fld1.setBackground(new java.awt.Color(204, 204, 204));
        ded_name_fld1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        ded_name_fld1.setForeground(new java.awt.Color(0, 0, 102));
        ded_name_fld1.setText("xxxxxxxxxxxxxxxxxx");
        ded_name_fld1.setBorder(null);
        ded_name_fld1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ded_name_fld1MouseClicked(evt);
            }
        });
        jPanel17.add(ded_name_fld1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, 170, -1));

        jSeparator16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.add(jSeparator16, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 170, 10));

        ded_value_fld1.setBackground(new java.awt.Color(204, 204, 204));
        ded_value_fld1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        ded_value_fld1.setForeground(new java.awt.Color(0, 0, 102));
        ded_value_fld1.setText("xxxxxxxxxxxxxxxxxx");
        ded_value_fld1.setBorder(null);
        ded_value_fld1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ded_value_fld1MouseClicked(evt);
            }
        });
        ded_value_fld1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ded_value_fld1ActionPerformed(evt);
            }
        });
        jPanel17.add(ded_value_fld1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 60, 170, -1));

        jSeparator21.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.add(jSeparator21, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 80, 170, 10));

        jLabel42.setForeground(new java.awt.Color(0, 0, 102));
        jLabel42.setText("Deduction Value:");
        jPanel17.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, -1, -1));

        add_ded1.setBackground(new java.awt.Color(0, 0, 51));
        add_ded1.setForeground(new java.awt.Color(255, 255, 255));
        add_ded1.setText("Add");
        add_ded1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_ded1MouseClicked(evt);
            }
        });
        jPanel17.add(add_ded1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 90, 150, -1));

        deductions_panel.add(jPanel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 100, 370, 130));

        body_main_panel.add(deductions_panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 10, 770, 445));

        overtime_panel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                overtime_panelMouseClicked(evt);
            }
        });
        overtime_panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(0, 0, 153));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel9.setFont(new java.awt.Font("Futura LT", 0, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Overtime rates");
        jPanel4.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 15, -1, -1));

        overtime_panel.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 850, 60));
        overtime_panel.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 250, -1, -1));

        jPanel16.setBackground(new java.awt.Color(204, 204, 204));
        jPanel16.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel45.setForeground(new java.awt.Color(0, 0, 102));
        jLabel45.setText("Overtime rates:");
        jPanel16.add(jLabel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        ovr_time_rates_fld.setBackground(new java.awt.Color(204, 204, 204));
        ovr_time_rates_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        ovr_time_rates_fld.setForeground(new java.awt.Color(0, 0, 102));
        ovr_time_rates_fld.setText("xxxxxxxxxxxxxxxxxx");
        ovr_time_rates_fld.setBorder(null);
        ovr_time_rates_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ovr_time_rates_fldMouseClicked(evt);
            }
        });
        ovr_time_rates_fld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ovr_time_rates_fldActionPerformed(evt);
            }
        });
        jPanel16.add(ovr_time_rates_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 30, 170, -1));

        jSeparator14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.add(jSeparator14, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 50, 170, 10));

        ovr_add_btn.setBackground(new java.awt.Color(0, 0, 51));
        ovr_add_btn.setForeground(new java.awt.Color(255, 255, 255));
        ovr_add_btn.setText("Add");
        ovr_add_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ovr_add_btnMouseClicked(evt);
            }
        });
        jPanel16.add(ovr_add_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 60, 140, -1));

        overtime_panel.add(jPanel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 370, 100));

        jPanel18.setBackground(new java.awt.Color(204, 204, 204));
        jPanel18.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add_overtime_jlist.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(add_overtime_jlist);

        jPanel18.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 320, 120));

        over_del_but.setBackground(new java.awt.Color(0, 0, 51));
        over_del_but.setForeground(new java.awt.Color(255, 255, 255));
        over_del_but.setText("Delete");
        over_del_but.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                over_del_butMouseClicked(evt);
            }
        });
        jPanel18.add(over_del_but, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 140, 140, -1));

        overtime_panel.add(jPanel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 370, 180));

        body_main_panel.add(overtime_panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 10, 770, 445));

        client_panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(0, 0, 153));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setFont(new java.awt.Font("Futura LT", 0, 24)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Add Client/Project");
        jPanel5.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 15, -1, -1));

        client_panel.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 850, 60));

        jPanel9.setBackground(new java.awt.Color(204, 204, 204));
        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        proj_name_fld.setBackground(new java.awt.Color(204, 204, 204));
        proj_name_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        proj_name_fld.setForeground(new java.awt.Color(0, 0, 102));
        proj_name_fld.setText("xxxxxxxxxxxxxxxxxx");
        proj_name_fld.setBorder(null);
        proj_name_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                proj_name_fldMouseClicked(evt);
            }
        });
        jPanel9.add(proj_name_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, 120, -1));

        jSeparator15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.add(jSeparator15, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 30, 120, 10));

        jLabel34.setForeground(new java.awt.Color(0, 0, 102));
        jLabel34.setText("End:");
        jPanel9.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 30, -1));

        add_proj_btn.setBackground(new java.awt.Color(0, 0, 51));
        add_proj_btn.setForeground(new java.awt.Color(255, 255, 255));
        add_proj_btn.setText("Add Project");
        add_proj_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_proj_btnMouseClicked(evt);
            }
        });
        jPanel9.add(add_proj_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 70, 130, -1));

        add_proj_end_jchooser.setBackground(new java.awt.Color(204, 204, 204));
        add_proj_end_jchooser.setForeground(new java.awt.Color(0, 0, 102));
        add_proj_end_jchooser.setDateFormatString("MM d, yyyy");
        jPanel9.add(add_proj_end_jchooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, 120, -1));

        jLabel37.setForeground(new java.awt.Color(0, 0, 102));
        jLabel37.setText("Start:");
        jPanel9.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        add_proj_start_jchooser.setBackground(new java.awt.Color(204, 204, 204));
        add_proj_start_jchooser.setForeground(new java.awt.Color(0, 0, 102));
        add_proj_start_jchooser.setDateFormatString("MM d, yyyy");
        jPanel9.add(add_proj_start_jchooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 120, -1));

        jLabel40.setForeground(new java.awt.Color(0, 0, 102));
        jLabel40.setText("Project name:");
        jPanel9.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        client_panel.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 100, 400, 140));

        jPanel10.setBackground(new java.awt.Color(204, 204, 204));
        jPanel10.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel35.setForeground(new java.awt.Color(0, 0, 102));
        jLabel35.setText("Client Name: ");
        jPanel10.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        jLabel36.setForeground(new java.awt.Color(0, 0, 102));
        jLabel36.setText("Client Address:");
        jPanel10.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, -1));

        jSeparator18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.add(jSeparator18, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 130, 10));

        add_client_btn.setBackground(new java.awt.Color(0, 0, 51));
        add_client_btn.setForeground(new java.awt.Color(255, 255, 255));
        add_client_btn.setText("Add Client");
        add_client_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_client_btnMouseClicked(evt);
            }
        });
        jPanel10.add(add_client_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 80, 130, -1));

        proj_client_address_fld1.setBackground(new java.awt.Color(204, 204, 204));
        proj_client_address_fld1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        proj_client_address_fld1.setForeground(new java.awt.Color(0, 0, 102));
        proj_client_address_fld1.setText("xxxxxxxxxxxxxxxxxx");
        proj_client_address_fld1.setBorder(null);
        proj_client_address_fld1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                proj_client_address_fld1MouseClicked(evt);
            }
        });
        jPanel10.add(proj_client_address_fld1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 50, 120, -1));

        proj_client_name_fld1.setBackground(new java.awt.Color(204, 204, 204));
        proj_client_name_fld1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        proj_client_name_fld1.setForeground(new java.awt.Color(0, 0, 102));
        proj_client_name_fld1.setText("xxxxxxxxxxxxxxxxxx");
        proj_client_name_fld1.setBorder(null);
        proj_client_name_fld1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                proj_client_name_fld1MouseClicked(evt);
            }
        });
        jPanel10.add(proj_client_name_fld1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 20, 120, -1));

        jSeparator26.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.add(jSeparator26, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 40, 130, 10));

        client_panel.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 300, 140));

        jPanel22.setBackground(new java.awt.Color(204, 204, 204));
        jPanel22.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add_proj_combolist.setForeground(new java.awt.Color(0, 0, 102));
        add_proj_combolist.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Perma", "Gaisano", "Ketkai", "SM", "Malls" }));
        jPanel22.add(add_proj_combolist, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 130, -1));

        jLabel55.setForeground(new java.awt.Color(0, 0, 102));
        jLabel55.setText("Link to Client  and Project ");
        jPanel22.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        jLabel60.setForeground(new java.awt.Color(0, 0, 102));
        jLabel60.setText("Client ID:");
        jPanel22.add(jLabel60, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        add_proj_combolist1.setForeground(new java.awt.Color(0, 0, 102));
        add_proj_combolist1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Perma", "Gaisano", "Ketkai", "SM", "Malls" }));
        jPanel22.add(add_proj_combolist1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 70, 130, -1));

        add_client_btn2.setBackground(new java.awt.Color(0, 0, 51));
        add_client_btn2.setForeground(new java.awt.Color(255, 255, 255));
        add_client_btn2.setText("Add Client");
        add_client_btn2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_client_btn2MouseClicked(evt);
            }
        });
        jPanel22.add(add_client_btn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 130, -1));

        jLabel62.setForeground(new java.awt.Color(0, 0, 102));
        jLabel62.setText("Project ID: ");
        jPanel22.add(jLabel62, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));

        client_panel.add(jPanel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, 300, 190));

        body_main_panel.add(client_panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 10, 770, 445));

        otherid_panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel6.setBackground(new java.awt.Color(0, 0, 153));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel17.setFont(new java.awt.Font("Futura LT", 0, 24)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Other ID's / Employee Position Rate");
        jPanel6.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 15, -1, -1));

        otherid_panel.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 850, 60));

        jPanel19.setBackground(new java.awt.Color(204, 204, 204));
        jPanel19.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel72.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel72.setForeground(new java.awt.Color(0, 0, 102));
        jLabel72.setText("Employee Position Rate");
        jPanel19.add(jLabel72, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 220, 30));

        emp_position_jlist.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        emp_position_jlist.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        add_other_id_JList2.setViewportView(emp_position_jlist);

        jPanel19.add(add_other_id_JList2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 290, 100));

        emp_pos_delete.setBackground(new java.awt.Color(0, 0, 51));
        emp_pos_delete.setForeground(new java.awt.Color(255, 255, 255));
        emp_pos_delete.setText("Delete");
        emp_pos_delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                emp_pos_deleteMouseClicked(evt);
            }
        });
        jPanel19.add(emp_pos_delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 140, 130, -1));

        otherid_panel.add(jPanel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 240, 320, 190));

        jPanel20.setBackground(new java.awt.Color(204, 204, 204));
        jPanel20.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel49.setForeground(new java.awt.Color(0, 0, 102));
        jLabel49.setText("Valid ID Name:");
        jPanel20.add(jLabel49, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, -1, -1));

        valid_id_name_fld2.setBackground(new java.awt.Color(204, 204, 204));
        valid_id_name_fld2.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        valid_id_name_fld2.setForeground(new java.awt.Color(0, 0, 102));
        valid_id_name_fld2.setText("xxxxxxxxxxxxxxxxxx");
        valid_id_name_fld2.setBorder(null);
        valid_id_name_fld2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                valid_id_name_fld2MouseClicked(evt);
            }
        });
        jPanel20.add(valid_id_name_fld2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 30, 170, -1));

        jSeparator24.setBackground(new java.awt.Color(255, 255, 255));
        jPanel20.add(jSeparator24, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 130, 10));

        other_id_add_btn1.setBackground(new java.awt.Color(0, 0, 51));
        other_id_add_btn1.setForeground(new java.awt.Color(255, 255, 255));
        other_id_add_btn1.setText("Add ID");
        other_id_add_btn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                other_id_add_btn1MouseClicked(evt);
            }
        });
        other_id_add_btn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                other_id_add_btn1ActionPerformed(evt);
            }
        });
        jPanel20.add(other_id_add_btn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 60, 130, -1));

        jLabel67.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel67.setForeground(new java.awt.Color(0, 0, 102));
        jLabel67.setText("Example: SSS, Pagibig...");
        jPanel20.add(jLabel67, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 220, 30));

        jLabel68.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel68.setForeground(new java.awt.Color(0, 0, 102));
        jLabel68.setText("Other ID");
        jPanel20.add(jLabel68, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 0, 220, 30));

        otherid_panel.add(jPanel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, 320, 140));

        jPanel24.setBackground(new java.awt.Color(204, 204, 204));
        jPanel24.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel65.setForeground(new java.awt.Color(0, 0, 102));
        jLabel65.setText("Position Name:");
        jPanel24.add(jLabel65, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        employee_position_name_fld.setBackground(new java.awt.Color(204, 204, 204));
        employee_position_name_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        employee_position_name_fld.setForeground(new java.awt.Color(0, 0, 102));
        employee_position_name_fld.setText("xxxxxxxxxxxxxxxxxx");
        employee_position_name_fld.setBorder(null);
        employee_position_name_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                employee_position_name_fldMouseClicked(evt);
            }
        });
        jPanel24.add(employee_position_name_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 30, 170, -1));

        jSeparator29.setBackground(new java.awt.Color(255, 255, 255));
        jPanel24.add(jSeparator29, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 170, 10));

        employee_position_btn.setBackground(new java.awt.Color(0, 0, 51));
        employee_position_btn.setForeground(new java.awt.Color(255, 255, 255));
        employee_position_btn.setText("Add ID");
        employee_position_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                employee_position_btnMouseClicked(evt);
            }
        });
        employee_position_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employee_position_btnActionPerformed(evt);
            }
        });
        jPanel24.add(employee_position_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 100, 130, -1));

        jSeparator30.setBackground(new java.awt.Color(255, 255, 255));
        jPanel24.add(jSeparator30, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 80, 170, 10));

        employee_position_rate_fld.setBackground(new java.awt.Color(204, 204, 204));
        employee_position_rate_fld.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        employee_position_rate_fld.setForeground(new java.awt.Color(0, 0, 102));
        employee_position_rate_fld.setText("xxxxxxxxxxxxxxxxxx");
        employee_position_rate_fld.setBorder(null);
        employee_position_rate_fld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                employee_position_rate_fldMouseClicked(evt);
            }
        });
        jPanel24.add(employee_position_rate_fld, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 60, 170, -1));

        jLabel69.setForeground(new java.awt.Color(0, 0, 102));
        jLabel69.setText("Position Rate;");
        jPanel24.add(jLabel69, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        jLabel70.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel70.setForeground(new java.awt.Color(0, 0, 102));
        jLabel70.setText("Employee Position Rate");
        jPanel24.add(jLabel70, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 220, 30));

        otherid_panel.add(jPanel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 90, 320, 140));

        jPanel26.setBackground(new java.awt.Color(204, 204, 204));
        jPanel26.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel71.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel71.setForeground(new java.awt.Color(0, 0, 102));
        jLabel71.setText("Other ID");
        jPanel26.add(jLabel71, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 220, 30));

        valid_other_id_list.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        valid_other_id_list.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        add_other_id_JList.setViewportView(valid_other_id_list);

        jPanel26.add(add_other_id_JList, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 290, 100));

        add_other_id_delete.setBackground(new java.awt.Color(0, 0, 51));
        add_other_id_delete.setForeground(new java.awt.Color(255, 255, 255));
        add_other_id_delete.setText("Delete");
        add_other_id_delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_other_id_deleteMouseClicked(evt);
            }
        });
        jPanel26.add(add_other_id_delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 140, 130, -1));

        otherid_panel.add(jPanel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 240, 320, 190));

        body_main_panel.add(otherid_panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 10, 770, 445));

        getContentPane().add(body_main_panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 160, 780, 470));

        menu.setBackground(new java.awt.Color(0, 0, 51));
        menu.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add_empoyee_btn2.setFont(new java.awt.Font("Futura LT", 0, 14)); // NOI18N
        add_empoyee_btn2.setForeground(new java.awt.Color(255, 255, 255));
        add_empoyee_btn2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/payroll/img/gear.png"))); // NOI18N
        add_empoyee_btn2.setText("  Settings");
        add_empoyee_btn2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_empoyee_btn2MouseClicked(evt);
            }
        });
        menu.add(add_empoyee_btn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 170, 30));

        hoverPanel_menu1_payslip.setBackground(new java.awt.Color(0, 0, 77));
        hoverPanel_menu1_payslip.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        generate_payslip_btn.setBackground(new java.awt.Color(0, 0, 51));
        generate_payslip_btn.setFont(new java.awt.Font("Futura LT", 0, 14)); // NOI18N
        generate_payslip_btn.setForeground(new java.awt.Color(255, 255, 255));
        generate_payslip_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/payroll/img/payslip_ico.png"))); // NOI18N
        generate_payslip_btn.setText("  Generate Payslip");
        generate_payslip_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                generate_payslip_btnMouseClicked(evt);
            }
        });
        hoverPanel_menu1_payslip.add(generate_payslip_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 140, 30));

        menu.add(hoverPanel_menu1_payslip, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 200, 30));

        hoverPanel_menu_employee.setBackground(new java.awt.Color(0, 0, 77));
        hoverPanel_menu_employee.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add_empoyee_btn.setFont(new java.awt.Font("Futura LT", 0, 14)); // NOI18N
        add_empoyee_btn.setForeground(new java.awt.Color(255, 255, 255));
        add_empoyee_btn.setIcon(new javax.swing.ImageIcon("C:\\Users\\maestrom4\\Documents\\NetBeansProjects\\payroll_sam_angel\\src\\payroll\\img\\employee_record.png")); // NOI18N
        add_empoyee_btn.setText("  Employee Record");
        add_empoyee_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_empoyee_btnMouseClicked(evt);
            }
        });
        hoverPanel_menu_employee.add(add_empoyee_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 150, 30));

        menu.add(hoverPanel_menu_employee, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 190, 200, 30));

        hoverPanel_menu_clinet_project.setBackground(new java.awt.Color(0, 0, 77));
        hoverPanel_menu_clinet_project.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add_client_btn1.setFont(new java.awt.Font("Futura LT", 0, 14)); // NOI18N
        add_client_btn1.setForeground(new java.awt.Color(255, 255, 255));
        add_client_btn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/payroll/employee_record.png"))); // NOI18N
        add_client_btn1.setText(" Client/Project");
        add_client_btn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_client_btn1MouseClicked(evt);
            }
        });
        hoverPanel_menu_clinet_project.add(add_client_btn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 130, 30));

        menu.add(hoverPanel_menu_clinet_project, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 200, 30));

        hoverPanel_menu_overtime.setBackground(new java.awt.Color(0, 0, 77));
        hoverPanel_menu_overtime.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add_overtime_rate.setFont(new java.awt.Font("Futura LT", 0, 12)); // NOI18N
        add_overtime_rate.setForeground(new java.awt.Color(255, 255, 255));
        add_overtime_rate.setIcon(new javax.swing.ImageIcon("C:\\Users\\maestrom4\\Documents\\NetBeansProjects\\payroll_sam_angel\\src\\payroll\\img\\over_time.png")); // NOI18N
        add_overtime_rate.setText("+ Overtime Rate");
        add_overtime_rate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_overtime_rateMouseClicked(evt);
            }
        });
        hoverPanel_menu_overtime.add(add_overtime_rate, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 0, 140, 30));

        menu.add(hoverPanel_menu_overtime, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 410, 200, 30));

        hoverPanel_menu_other_id.setBackground(new java.awt.Color(0, 0, 77));
        hoverPanel_menu_other_id.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add_otherid_btn.setFont(new java.awt.Font("Futura LT", 0, 12)); // NOI18N
        add_otherid_btn.setForeground(new java.awt.Color(255, 255, 255));
        add_otherid_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/payroll/employee_record.png"))); // NOI18N
        add_otherid_btn.setText("+ ID's /Position Rate");
        add_otherid_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_otherid_btnMouseClicked(evt);
            }
        });
        hoverPanel_menu_other_id.add(add_otherid_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 0, 160, 30));

        menu.add(hoverPanel_menu_other_id, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 450, 200, 30));

        hoverPanel_menu_deductions.setBackground(new java.awt.Color(0, 0, 77));
        hoverPanel_menu_deductions.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add_deductions_btn.setFont(new java.awt.Font("Futura LT", 0, 12)); // NOI18N
        add_deductions_btn.setForeground(new java.awt.Color(255, 255, 255));
        add_deductions_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/payroll/img/add_deductions.png"))); // NOI18N
        add_deductions_btn.setText("+ Deductions");
        add_deductions_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_deductions_btnMouseClicked(evt);
            }
        });
        hoverPanel_menu_deductions.add(add_deductions_btn, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 0, 120, 30));

        menu.add(hoverPanel_menu_deductions, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 370, 200, 30));

        hoverPanel_menu_other_id1.setBackground(new java.awt.Color(0, 0, 77));
        hoverPanel_menu_other_id1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add_otherid_btn1.setFont(new java.awt.Font("Futura LT", 0, 12)); // NOI18N
        add_otherid_btn1.setForeground(new java.awt.Color(255, 255, 255));
        add_otherid_btn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/payroll/employee_record.png"))); // NOI18N
        add_otherid_btn1.setText("  Add Other ID misc.");
        add_otherid_btn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                add_otherid_btn1MouseClicked(evt);
            }
        });
        hoverPanel_menu_other_id1.add(add_otherid_btn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, 140, 30));

        menu.add(hoverPanel_menu_other_id1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 450, 200, 30));

        getContentPane().add(menu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 200, 630));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void generate_payslip_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_generate_payslip_btnMouseClicked
        // TODO add your handling code here:
            
            this.overtime_panel.setVisible(false);
            this.deductions_panel.setVisible(false);
            this.client_panel.setVisible(false);
            this.payslip_panel.setVisible(true);
            this.employee_panel.setVisible(false);
            this.otherid_panel.setVisible(false);
//            this.paySlipcomboDeductions();
            this.paySlipcomboClient();
            this.paySlipcomboProject();
            this.paySlipcomboPosition();
            this.paySlipcomboOvertime();
//            this.paySlipJlistDeductions();
//            this.hoverPanel_menu.setB
            int menuid = 1;
            this.menuHover(menuid);
            this.modelPayslipDeductionsfunc(); // reload payslip deduction table
        
    }//GEN-LAST:event_generate_payslip_btnMouseClicked

    private void add_empoyee_btn2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_empoyee_btn2MouseClicked
 
    }//GEN-LAST:event_add_empoyee_btn2MouseClicked

    private void add_otherid_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_otherid_btnMouseClicked
        // TODO add your handling code here:
        this.overtime_panel.setVisible(false);
        this.deductions_panel.setVisible(false);
        this.client_panel.setVisible(false);
        this.payslip_panel.setVisible(false);
        this.employee_panel.setVisible(false);
        this.otherid_panel.setVisible(true);
        
        this.other_valid_id_add_jlist();
        int menuid = 6;
        this.menuHover(menuid);
        this.emp_position_jlist();
   

    }//GEN-LAST:event_add_otherid_btnMouseClicked
  
    public void other_valid_id_add_jlist(){
        
//        list[0] = "aq";
        String[] list;
      
        int numRows=0;
        try{
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            String sql2 = "SELECT count(*) FROM "+this.dbtables[4];
            PreparedStatement st2 = this.conn.prepareStatement(sql2);
//            st.setString(1, searchid);
            ResultSet res2 = st2.executeQuery(); 
            if(res2.next()){
                 numRows = res2.getInt(1);
                this.tempList_other_id= new String[numRows];
//                System.out.println(numRows+" ***********");
                
            }
//            else {
//                
//            }
  
        }catch(Exception e){
            e.printStackTrace();
        }
        
        try{
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);

            String sql = "SELECT * FROM "+this.dbtables[4];
            PreparedStatement st = this.conn.prepareStatement(sql);
//            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            int i = 0;
            while (res.next()) { 
                this.tempList_other_id[i] = res.getString("emp_ids_office_name");
//                String msg = res.getString("ded_value");
//                System.out.println(id + "\t" + msg);
                i++;
            }
//            System.out.println(numRows+" ***********");
            this.valid_other_id_list.setListData(this.tempList_other_id);
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
    }
    public void emp_position_jlist(){
        
//        list[0] = "aq";
        String[] list;
      
        int numRows=0;
        try{
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            String sql2 = "SELECT count(*) FROM "+this.dbtables[8];
            PreparedStatement st2 = this.conn.prepareStatement(sql2);
//            st.setString(1, searchid);
            ResultSet res2 = st2.executeQuery(); 
            if(res2.next()){
                 numRows = res2.getInt(1);
                this.tempList_other_id= new String[numRows];
//                System.out.println(numRows+" ***********");
                
            }
//            else {
//                
//            }
  
        }catch(Exception e){
            e.printStackTrace();
        }
        
        try{
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);

            String sql = "SELECT * FROM "+this.dbtables[8];
            PreparedStatement st = this.conn.prepareStatement(sql);
//            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            int i = 0;
            while (res.next()) { 
                this.tempList_other_id[i] = res.getString("pos_name")+", "+res.getString("pos_salary");
//                String msg = res.getString("ded_value");
//                System.out.println(id + "\t" + msg);
                i++;
            }
//            System.out.println(numRows+" ***********");
            this.emp_position_jlist.setListData(this.tempList_other_id);
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
    }
    public void add_overtime_jlist_func(){
        
//        list[0] = "aq";
        String[] list;
      
        int numRows=0;
        try{
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            String sql2 = "SELECT count(*) FROM "+this.dbtables[7];
            PreparedStatement st2 = this.conn.prepareStatement(sql2);
//            st.setString(1, searchid);
            ResultSet res2 = st2.executeQuery(); 
            if(res2.next()){
                 numRows = res2.getInt(1);
                this.tempList_other_id= new String[numRows];
//                System.out.println(numRows+" ***********");
                
            }
//            else {
//                
//            }
  
        }catch(Exception e){
            e.printStackTrace();
        }
        
        try{
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);

            String sql = "SELECT * FROM "+this.dbtables[7];
            PreparedStatement st = this.conn.prepareStatement(sql);
//            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            int i = 0;
            while (res.next()) { 
                this.tempList_other_id[i] = res.getString("ovr_time_pay");
//                String msg = res.getString("ded_value");
//                System.out.println(id + "\t" + msg);
                i++;
            }
//            System.out.println(numRows+" ***********");
            this.add_overtime_jlist.setListData(this.tempList_other_id);
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
    }
    public void add_deductions_jlist_func(){
        
//        list[0] = "aq";
        String[] list;
      
        int numRows=0;
        try{
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            String sql2 = "SELECT count(*) FROM "+this.dbtables[2];
            PreparedStatement st2 = this.conn.prepareStatement(sql2);
//            st.setString(1, searchid);
            ResultSet res2 = st2.executeQuery(); 
            if(res2.next()){
                 numRows = res2.getInt(1);
                this.tempList_other_id= new String[numRows];
//                System.out.println(numRows+" ***********");
                
            }
//            else {
//                
//            }
  
        }catch(Exception e){
            e.printStackTrace();
        }
        
        try{
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);

            String sql = "SELECT * FROM "+this.dbtables[2];
            PreparedStatement st = this.conn.prepareStatement(sql);
//            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            int i = 0;
            while (res.next()) { 
                this.tempList_other_id[i] = res.getString("ded_name")+": \t ["+res.getString("ded_value")+"]";
//                  this.tempList_other_id[i] = res.getString("ded_name");
//                String msg = res.getString("ded_value");
//                System.out.println(id + "\t" + msg);
                i++;
            }
//            System.out.println(numRows+" ***********");
            this.add_deduction_jlist.setListData(this.tempList_other_id);
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
    }
    public boolean check_other_id_exist(String name){
        boolean temp = false;
        try{
            boolean result=false;
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);

            String sql = "SELECT * FROM "+this.dbtables[4]+" WHERE emp_ids_office_name=?";
            PreparedStatement st = this.conn.prepareStatement(sql);
            st.setString(1, name);
            ResultSet res = st.executeQuery(); 
            System.out.println("layer 1");
            if(res.next()){
                temp= true;
                
                
//                if(res.getInt(1)==1){
                    System.out.println("layer 2:"+res.getInt(1));
//                }else {
                    System.out.println("layer 3:"+res.getInt(1));
//                }
            }
            else {
                
            }
                System.out.println("layer 4 pass");
           
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return temp;
    }
    public boolean delete_other_id(String name){
        boolean temp = false;
        try{
            boolean result=false;
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);

            String sql = "DELETE FROM "+this.dbtables[4]+" WHERE emp_ids_office_name=?";
            PreparedStatement st = this.conn.prepareStatement(sql);
            st.setString(1, name);
            st.executeUpdate();
            System.out.println("layer 1");
//            if(res.next()){
//                temp= true;
//                
////                System.out.println("layer 2:"+res.getInt(1));
//            }else {
//                temp = false;
////                System.out.println("layer 3:"+res.getInt(1));
//            }
//                System.out.println("layer 4 pass");
           
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return temp;
    }
    public boolean delete_overtime_id(String name){
        boolean temp = false;
        try{
            boolean result=false;
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);

            String sql = "DELETE FROM "+this.dbtables[7]+" WHERE ovr_time_pay=?";
            PreparedStatement st = this.conn.prepareStatement(sql);
            st.setDouble(1, Double.parseDouble(name));
            st.executeUpdate();
            System.out.println("layer 1");
//            if(res.next()){
//                temp= true;
//                
////                System.out.println("layer 2:"+res.getInt(1));
//            }else {
//                temp = false;
////                System.out.println("layer 3:"+res.getInt(1));
//            }
//                System.out.println("layer 4 pass");
           
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return temp;
    }
    public void delete_emp_position_id(String data){
        boolean temp = false;
        try{
            boolean result=false;
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
            System.out.println("db: "+this.dbtables[8]);
            String sql = "DELETE FROM "+this.dbtables[8]+" WHERE pos_name=?";
            PreparedStatement st = this.conn.prepareStatement(sql);
            st.setString(1, data);
            st.executeUpdate();
           
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
//        return temp;
    }
    public boolean delete_deduction_id(String data){
        boolean temp = false;
        try{
            boolean result=false;
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
            System.out.println("db: "+this.dbtables[2]);
            String sql = "DELETE FROM "+this.dbtables[2]+" WHERE ded_name=?";
            PreparedStatement st = this.conn.prepareStatement(sql);
            st.setString(1, data);
            st.executeUpdate();
           
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return temp;
    }
    private void add_deductions_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_deductions_btnMouseClicked
        // TODO add your handling code here:
        this.overtime_panel.setVisible(false);
        this.deductions_panel.setVisible(true);
        this.client_panel.setVisible(false);
        this.payslip_panel.setVisible(false);
        this.employee_panel.setVisible(false);
        this.otherid_panel.setVisible(false);
        this.add_deductions_jlist_func();
        int menuid = 4;
        this.menuHover(menuid);
    }//GEN-LAST:event_add_deductions_btnMouseClicked

    private void add_overtime_rateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_overtime_rateMouseClicked
        // TODO add your handling code here:
        this.overtime_panel.setVisible(true);
        this.deductions_panel.setVisible(false);
        this.client_panel.setVisible(false);
        this.payslip_panel.setVisible(false);
        this.employee_panel.setVisible(false);
        this.otherid_panel.setVisible(false);
        this.add_overtime_jlist_func();
        int menuid = 5;
        this.menuHover(menuid);
        
    }//GEN-LAST:event_add_overtime_rateMouseClicked

    private void add_client_btn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_client_btn1MouseClicked
        // TODO add your handling code here:
        this.overtime_panel.setVisible(false);
        this.deductions_panel.setVisible(false);
        this.client_panel.setVisible(true);
        this.payslip_panel.setVisible(false);
        this.employee_panel.setVisible(false);
        this.otherid_panel.setVisible(false);
        int menuid =3;
        this.menuHover(menuid);
        this.paySlipcomboClient();
        this.paySlipcomboProject();
    }//GEN-LAST:event_add_client_btn1MouseClicked

    private void ovr_add_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ovr_add_btnMouseClicked
        // TODO add your handling code here:
        this.add_overtime();
        this.add_overtime_jlist_func();
        this.ovr_time_rates_fld.setText("");
    }//GEN-LAST:event_ovr_add_btnMouseClicked

    private void add_client_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_client_btnMouseClicked
        // TODO add your handling code here:
        
       this.add_client2();
       this.paySlipcomboClient();
       
        
    }//GEN-LAST:event_add_client_btnMouseClicked
    
    private void add_proj_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_proj_btnMouseClicked
        // TODO add your handling code here:
        this.add_proj();
//        this.add_link_proj_to_client();
        this.paySlipcomboProject();
        this.proj_name_fld.setText("");
    }//GEN-LAST:event_add_proj_btnMouseClicked

    private void proj_name_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_proj_name_fldMouseClicked
        // TODO add your handling code here:
        this.proj_name_fld.setText("");
    }//GEN-LAST:event_proj_name_fldMouseClicked

    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        // TODO add your handling code here:
        this.add_emp();
        this.emp_id_fld.setText("");
        this.emp_fname_fld.setText("");
        this.emp_lname_fld.setText("");
        this.emp_add_fld.setText("");
        this.emp_standing_fld.setText("");
        
    }//GEN-LAST:event_jButton3MouseClicked

    private void emp_id_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emp_id_fldMouseClicked
        // TODO add your handling code here:
        this.emp_id_fld.setText("");
    }//GEN-LAST:event_emp_id_fldMouseClicked

    private void emp_fname_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emp_fname_fldMouseClicked
        // TODO add your handling code here:
        this.emp_fname_fld.setText("");
    }//GEN-LAST:event_emp_fname_fldMouseClicked

    private void emp_lname_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emp_lname_fldMouseClicked
        // TODO add your handling code here:
        this.emp_lname_fld.setText("");
    }//GEN-LAST:event_emp_lname_fldMouseClicked

    private void emp_add_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emp_add_fldMouseClicked
        // TODO add your handling code here:
        this.emp_add_fld.setText("");
    }//GEN-LAST:event_emp_add_fldMouseClicked

    private void emp_standing_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emp_standing_fldMouseClicked
        // TODO add your handling code here:
        this.emp_standing_fld.setText("");
    }//GEN-LAST:event_emp_standing_fldMouseClicked

    private void update_clientMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_update_clientMouseClicked
        // TODO add your handling code here:
       this.update_client();
           
    }//GEN-LAST:event_update_clientMouseClicked

    private void overtime_panelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_overtime_panelMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_overtime_panelMouseClicked

    private void ovr_time_rates_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ovr_time_rates_fldMouseClicked
        // TODO add your handling code here:
        this.ovr_time_rates_fld.setText("");
        
    }//GEN-LAST:event_ovr_time_rates_fldMouseClicked

    private void ovr_time_rates_fldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ovr_time_rates_fldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ovr_time_rates_fldActionPerformed

    private void emp_standing_fldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emp_standing_fldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_emp_standing_fldActionPerformed

    private void emp_id_fldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emp_id_fldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_emp_id_fldActionPerformed

    private void over_del_butMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_over_del_butMouseClicked
        // TODO add your handling code here:
        String temp = this.add_overtime_jlist.getSelectedValue();
        this.delete_overtime_id(temp);
        this.add_overtime_jlist_func();
        JOptionPane.showMessageDialog(null, "Successfully removed record!");
    }//GEN-LAST:event_over_del_butMouseClicked

    private void emp_update_fname_fldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_emp_update_fname_fldKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_emp_update_fname_fldKeyPressed

    private void emp_update_emp_id_fldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_emp_update_emp_id_fldKeyPressed
        // TODO add your handling code here:
        try{
            System.out.println(evt.getKeyCode());
            String[] temp;
            boolean temp2;
            if(evt.getKeyCode()==10){
                System.out.println("Enter is pressed!");
                temp2 = this.search_emp_id(this.emp_update_emp_id_fld.getText());
                if(!temp2){
                    JOptionPane.showMessageDialog(null, "No Employee record found!");
                    return;
                }else {
                    temp = this.search_emp_id_with_result(this.emp_update_emp_id_fld.getText());
                
//                    System.out.println(Arrays.toString(temp));
                    this.emp_update_fname_fld.setText(temp[1]);
                    this.emp_update_lname_fld.setText(temp[2]);
                    this.emp_update_address_fld.setText(temp[4]);
                    this.emp_update_standing_fld.setText(temp[5]);
                    /**
                     * Timestamp to date
                     */
                    String x = temp[3];  
                    //from timestamp to date to dateformat
                    long foo = Long.parseLong(x);
                    System.out.println(x + "\n" + foo);

                    Date date = new Date(foo*1000);
    //   
                    this.emp_update_calendar.setDate(date);
                }
                
  
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }//GEN-LAST:event_emp_update_emp_id_fldKeyPressed

    private void emp_lname_fldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emp_lname_fldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_emp_lname_fldActionPerformed

    private void emp_bday_calendarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emp_bday_calendarMouseClicked
        // TODO add your handling code here:
        System.out.println("click calendar!");
    }//GEN-LAST:event_emp_bday_calendarMouseClicked

    private void emp_update_emp_id_fldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_emp_update_emp_id_fldFocusGained
        // TODO add your handling code here:
        this.emp_update_emp_id_fld.setText("");
    }//GEN-LAST:event_emp_update_emp_id_fldFocusGained

    private void emp_update_address_fldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_emp_update_address_fldFocusGained
        // TODO add your handling code here:
        this.emp_update_address_fld.setText("");
    }//GEN-LAST:event_emp_update_address_fldFocusGained

    private void emp_update_fname_fldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_emp_update_fname_fldFocusGained
        // TODO add your handling code here:
        this.emp_update_fname_fld.setText("");
    }//GEN-LAST:event_emp_update_fname_fldFocusGained

    private void emp_update_standing_fldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_emp_update_standing_fldFocusGained
        // TODO add your handling code here:
        this.emp_update_standing_fld.setText("");
    }//GEN-LAST:event_emp_update_standing_fldFocusGained

    private void emp_update_lname_fldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_emp_update_lname_fldFocusGained
        // TODO add your handling code here:
        this.emp_update_lname_fld.setText("");
    }//GEN-LAST:event_emp_update_lname_fldFocusGained

    private void emp_update_calendarFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_emp_update_calendarFocusGained
        // TODO add your handling code here:
        
    }//GEN-LAST:event_emp_update_calendarFocusGained

    private void delete_clickMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_delete_clickMouseClicked
        // TODO add your handling code here:
        this.emp_update_delete_id(this._currentUpdateId);
        this.emp_update_emp_id_fld.setText("");
        this.emp_update_fname_fld.setText("");
        this.emp_update_lname_fld.setText("");

        this.emp_update_address_fld.setText("");
        this.emp_update_standing_fld.setText("");
    }//GEN-LAST:event_delete_clickMouseClicked

    private void valid_id_name_fld2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_valid_id_name_fld2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_valid_id_name_fld2MouseClicked

    private void other_id_add_btn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_other_id_add_btn1MouseClicked
        // TODO add your handling code here: 
        try{
        
            if(this.valid_id_name_fld2.getText().equals("")){ 
                JOptionPane.showMessageDialog(null, "Don't leave blanks");
    //            return;
            }else {
//                System.out.println("result: "+);
                if(this.check_other_id_exist(this.valid_id_name_fld2.getText())){
                    JOptionPane.showMessageDialog(null, "Duplicate entry!");
                    return;
                }
                this.add_other_id();
                this.other_valid_id_add_jlist();
                JOptionPane.showMessageDialog(null, "Successfully added to list!");
            }
//        }
////        catch(NullPointerException e2){
////            JOptionPane.showMessageDialog(null, "Don't leave blanks");
////            return;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
        
    }//GEN-LAST:event_other_id_add_btn1MouseClicked

    private void other_id_add_btn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_other_id_add_btn1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_other_id_add_btn1ActionPerformed

    private void add_other_id_deleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_other_id_deleteMouseClicked
        String temp = this.valid_other_id_list.getSelectedValue();
        System.out.println(temp+" : temp");
        this.delete_other_id(temp);
        this.other_valid_id_add_jlist();
        JOptionPane.showMessageDialog(null, "Successfully Deleted!");
    }//GEN-LAST:event_add_other_id_deleteMouseClicked

    private void ded_name_fld1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ded_name_fld1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_ded_name_fld1MouseClicked

    private void ded_value_fld1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ded_value_fld1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_ded_value_fld1MouseClicked

    private void ded_value_fld1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ded_value_fld1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ded_value_fld1ActionPerformed

    private void add_ded1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_ded1MouseClicked
        // TODO add your handling code here:
        this.add_deduction();
        this.add_deductions_jlist_func();
        JOptionPane.showMessageDialog(null, "Successfully Added deuctions!");
        this.ded_name_fld1.setText("");
        this.ded_value_fld1.setText("");
    }//GEN-LAST:event_add_ded1MouseClicked

    private void delete_ded_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_delete_ded_btnMouseClicked
            // TODO add your handling code here:
        String temp = this.add_deduction_jlist.getSelectedValue();
        String[] parts = temp.split(":");
//        System.out.println(Arrays.toString(parts)+" : temp");
//        System.out.println(parts[1]+" : temp");
        this.delete_deduction_id(parts[0]);
        this.add_deductions_jlist_func();
        JOptionPane.showMessageDialog(null, "Successfully Deleted!");
        
        
    }//GEN-LAST:event_delete_ded_btnMouseClicked

    private void gen_payslip_emp_id_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gen_payslip_emp_id_fldMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_gen_payslip_emp_id_fldMouseClicked

    private void gen_payslip_emp_id_fldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_gen_payslip_emp_id_fldKeyPressed
        // TODO add your handling code here:
        try{
            System.out.println(evt.getKeyCode());
            String[] temp;
            boolean temp2;
            if(evt.getKeyCode()==10){
                System.out.println("Enter is pressed!");
                temp2 = this.search_emp_id(this.gen_payslip_emp_id_fld.getText());
                if(!temp2){
                    JOptionPane.showMessageDialog(null, "No Employee record found!");
                    return;
                }else {
                    temp = this.search_emp_id_with_result(this.gen_payslip_emp_id_fld.getText());
                
//                    System.out.println(Arrays.toString(temp));
                    this.payslip_fullname_lbl1.setText(temp[1]+" "+temp[2]);

                }
                
  
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }//GEN-LAST:event_gen_payslip_emp_id_fldKeyPressed

    private void add_empoyee_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_empoyee_btnMouseClicked
        // TODO add your handling code here:
        
        this.overtime_panel.setVisible(false);
        this.deductions_panel.setVisible(false);
        this.client_panel.setVisible(false);
        this.payslip_panel.setVisible(false);
        this.employee_panel.setVisible(true);
        this.otherid_panel.setVisible(false);
        int menuid = 2;
        this.menuHover(menuid);
        this.paySlipcomboProject();
        this.paySlipcomboClient();
//        this.hoverPanel_menu_employee.setLocation(0, 260);
    }//GEN-LAST:event_add_empoyee_btnMouseClicked

    private void gen_payslip_emp_hoursworkedMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gen_payslip_emp_hoursworkedMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_gen_payslip_emp_hoursworkedMouseClicked

    private void gen_payslip_emp_hoursworkedKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_gen_payslip_emp_hoursworkedKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_gen_payslip_emp_hoursworkedKeyPressed

    private void paylist_deduction_tblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_paylist_deduction_tblMouseClicked
        // TODO add your handling code here:
//        if(evt.getClickCount()==2){
            int rowData = paylist_deduction_tbl.getSelectedRow();
            int colData = paylist_deduction_tbl.getSelectedColumn();
//            System.out.println("Selected row: "+rowData);
//            System.out.println("Selected col: "+colData);//
//            System.out.println("Selected data: "+this.tempPayslipDeductions[rowData][1]);
            String temp = this.payslip_ded_fld.getText();
            
            this.payslip_ded_fld.setText(temp+this.tempPayslipDeductions[rowData][1]+",");
            this.payslipDeductionTblSelected+=this.tempPayslipDeductions[rowData][0]+",";
            System.out.println("this.payslipDeductionTblSelected: "+this.payslipDeductionTblSelected);
//        }
    }//GEN-LAST:event_paylist_deduction_tblMouseClicked

    private void gen_payslip_emp_ot_thoursMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gen_payslip_emp_ot_thoursMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_gen_payslip_emp_ot_thoursMouseClicked

    private void gen_payslip_emp_ot_thoursKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_gen_payslip_emp_ot_thoursKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_gen_payslip_emp_ot_thoursKeyPressed
    public int client_project_id(){
        int result=0;
        try{
            Class.forName(driver).newInstance();
//            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
          this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            String sql2 = "SELECT tbl_client_has_tbl_project_id FROM tbl_client_has_tbl_project WHERE tbl_client_client_id=? AND tbl_project_prj_id=?";
            PreparedStatement st = this.conn.prepareStatement(sql2);
            st.setString(1, this.payslip_combox_client1.getItemAt(this.payslip_combox_client1.getSelectedIndex()));
            st.setString(2, this.payslip_combox_project1.getItemAt(this.payslip_combox_project1.getSelectedIndex()));
            ResultSet res = st.executeQuery(); 
            while(res.next()){
                result=Integer.parseInt(res.getString("tbl_client_has_tbl_project_id"));
                System.out.println("+++++++++++++++++++"+result); 
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
        
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return result;
    }
    public void insertPayslipNetpayDeductions(){
        try{
             Class.forName(driver).newInstance();
//            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
            String[] tempg =  this.payslipDeductionTblSelected.split(",");
            for(int i=0;i<tempg.length;i++){
                System.out.println(Arrays.toString(tempg)+"333333333333333333333");
            
                String sql3 = "INSERT INTO "+this.dbtables[10]+" (tbl_netpay_net_pay_id,tbl_deduction_ded_id) VALUES (?,?)";
                PreparedStatement st = this.conn.prepareStatement(sql3);
                st.setInt(1,this.lastgenNetPayId);
                st.setInt(2, Integer.parseInt(tempg[i]));

                int rowsInserted2 = st.executeUpdate();
                if (rowsInserted2 > 0) {
                    System.out.println("A new user was inserted successfully!");
                }
//                
//                a++;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
       
    }
    private void generate_payslip_menu_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_generate_payslip_menu_btnMouseClicked
        // TODO add your handling code here:
        String empid = this.gen_payslip_emp_id_fld.getText();
        String[] fullname = this.payslip_fullname_lbl1.getText().split(" ");
        String client = this.payslip_combox_client1.getItemAt(this.payslip_combox_client1.getSelectedIndex());
        String project = this.payslip_combox_project1.getItemAt(this.payslip_combox_project1.getSelectedIndex());
        String[] deductions = this.payslip_ded_fld.getText().split(",");
        int x = deductions.length;
        double[]temptt=new double[x];
        for(int i=0; i<x;i++){
            temptt[i]=Double.parseDouble(deductions[i]);
        }
        String temp =this.gen_payslip_emp_hoursworked.getText();
        
        if(temp.equals("")){
//            System.out.println("Null detected! ");
            JOptionPane.showMessageDialog(null, "Worked hours is blank!");
            return;
        }
        temp =this.gen_payslip_emp_ot_thours.getText();
        if(temp.equals("")){
//            System.out.println("Null detected! ");
            JOptionPane.showMessageDialog(null, "Over time hours is blank!");
            return;
        }
        int hoursworked = Integer.parseInt(this.gen_payslip_emp_hoursworked.getText());
        int otHours = Integer.parseInt(this.gen_payslip_emp_ot_thours.getText());
        
//        double otRate = Double.parseDouble(this.payslip_combox_overtime1.getItemAt(this.payslip_combox_overtime1.getSelectedIndex()));
        String clientId = this.payslip_combox_client1.getItemAt(this.payslip_combox_client1.getSelectedIndex());
//        System.out.println(" *************");
        String posId = this.payslip_combox_position1.getItemAt(this.payslip_combox_position1.getSelectedIndex()).split(",")[0];    
        String position = this.payslip_combox_position1.getItemAt(this.payslip_combox_position1.getSelectedIndex()).split(",")[1];
        String posRateVal = this.payslip_combox_position1.getItemAt(this.payslip_combox_position1.getSelectedIndex()).split(",")[2];
        
//        String position = posRate[0].split(",")[0];
//        System.out.println("************ posId : "+posId);
//        System.out.println("************ posRateVal : "+posRateVal);
//        JOptionPane.showMessageDialog(null, "pass here!*********! "+position);
//        double posRateVal = Double.parseDouble(posRate[1]);
//        
//        System.out.println("************: "+posRateVal);
        
//        String posRate = this.payslip_combox_position1.getItemAt(this.payslip_combox_position1.getSelectedIndex());
        double tempx = Double.parseDouble(posRateVal);
//        System.out.println("Check values *******"+Arrays.toString(temptt));
        this.ps = new Payslip(empid,fullname[0],fullname[1], client, project, temptt, hoursworked,otHours,position,tempx);
//        System.out.println("Grosspay: "+this.ps.grossPay());
//        System.out.println("Total deductions: "+this.ps.totalDeductions());
//        System.out.println("Netpay: "+this.ps.netPay());
//        String temp = Double.parseDouble().toString(this.this.ps.grossPay());
        double number = this.ps.grossPay();
        String numberAsString = Double.toString(number);
        this.payslip_grosspay_res_fld.setText(Double.toString(this.ps.grossPay()));
        this.payslip_deductions_res_fld.setText(Double.toString(this.ps.totalDeductions()));
        this.payslip_netpay_res_fld.setText(Double.toString(this.ps.netPay()));
        
        try {
            Class.forName(driver).newInstance();
//            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
          this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            
            
//         tbl_deduction_ded_id	tbl_overtime_ovr_time_id	tbl_position_pos_id	tbl_client_has_tbl_project_tbl_client_has_tbl_projectcol	tbl_emp_prof_id	tbl_emp_prof_emp_id	tbl_hours_work	tbl_overtime_hours	timestamp
//            long timestamp = System.currentTimeMillis()/1000;
//            String timestamp2 = Long.toString(timestamp);

            /**
             * Insert data to netpay
             */
            int overtime = Integer.parseInt(this.payslip_combox_overtime1.getItemAt(this.payslip_combox_overtime1.getSelectedIndex()).split(",")[0]);
            double overtime2 = Double.parseDouble(this.payslip_combox_overtime1.getItemAt(this.payslip_combox_overtime1.getSelectedIndex()).split(",")[1]);
//            int overtime2 = Integer.parseInt(this.payslip_combox_overtime1.getItemAt(this.payslip_combox_overtime1.getSelectedIndex()));
            System.out.println("------overtime----"+overtime);
            long timestamp = System.currentTimeMillis()/1000;
            String timestamp2 = Long.toString(timestamp);
            String sql = "INSERT INTO "+this.dbtables[6]+" (tbl_overtime_ovr_time_id,"
                    + "tbl_position_pos_id,tbl_prj_client_id,"
                    + "tbl_emp_prof_emp_id,tbl_hours_worked,tbl_over_time_hours, "
                    + "timestamp) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement statement = this.conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1,overtime);
            statement.setInt(2, Integer.parseInt(posId));
            statement.setInt(3,  this.client_project_id());
            statement.setString(4,  this.gen_payslip_emp_id_fld.getText());
            statement.setString(5,  this.gen_payslip_emp_hoursworked.getText());
            statement.setString(6,  this.gen_payslip_emp_ot_thours.getText());
            statement.setString(7,  timestamp2);
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
            }
            /**
             * Gets last generated ID
             */
            ResultSet rs = statement.getGeneratedKeys();
            int generatedKey = 0;
            if (rs.next()) {
                this.lastgenNetPayId = rs.getInt(1);
            }
             /**
             * Insert data to netpay deductions table
             */
            String[] dedList = this.payslip_ded_fld.getText().split(",");
            System.out.println("********************"+Arrays.toString(dedList));
//            dedList.
            /***
             * Add netpay deductions with id
             */
            this.insertPayslipNetpayDeductions();
//            pdfwriter2(String cname, String companyAdd, String datep,String tslip,String empId,String clienN,String listded,int hWorked,int otHours,double otRate,String pRp){
            String compName = "ANGEL & SAM CONST. & GEN. MDSE.";
            String compAdd = "2830 Macajalar, Camaman-an, Cagayan de Oro City";
            String clientName = this.payslip_combox_client1.getItemAt(this.payslip_combox_client1.getSelectedIndex()).split(",")[1];
            String clientID = this.payslip_combox_client1.getItemAt(this.payslip_combox_client1.getSelectedIndex()).split(",")[0];
//            String position = this.payslip_combox_position1.getItemAt(this.payslip_combox_position1.getSelectedIndex()).split(",")[1];
            int empworkHours =Integer.parseInt(this.gen_payslip_emp_hoursworked.getText());
            int empotHours = Integer.parseInt(this.gen_payslip_emp_ot_thours.getText());
            String grosspay =this.payslip_grosspay_res_fld.getText();
            String deductionsList = this.payslip_deductions_res_fld.getText();
            String netpay = this.payslip_netpay_res_fld.getText();
            String tempxy = grosspay+","+deductionsList+","+netpay;
            pdfwriter2 pdf = new pdfwriter2(compName, compAdd, this.todaysDate(), "YOUR PAYSLIP", clientId, clientName, this.payslip_ded_fld.getText(), empworkHours,empotHours,overtime2,posRateVal+","+posId+","+position,this.lastgenNetPayId, this.payslip_fullname_lbl1.getText(),this.gen_payslip_emp_id_fld.getText(),tempxy);
            
            
          
//            Date startStamp = sdf.parse(date);
//            String date1 = sdf.format(this.add_proj_end_jchooser.getDate());
//            Date endStamp = sdf.parse(date1);
//            //to timestamp
//            long millis = startStamp.getTime()/1000;
//            String startStamp2 = Long.toString(millis);
//            long millis2 = endStamp.getTime()/1000;
//            String endStamp2 = Long.toString(millis2);
            
           
//           

            
            }catch(SQLException e){
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally{
                try{
                    if(this.conn!=null){
                        this.conn.close();
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
    }//GEN-LAST:event_generate_payslip_menu_btnMouseClicked
    
    private void proj_client_address_fld1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_proj_client_address_fld1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_proj_client_address_fld1MouseClicked

    private void proj_client_name_fld1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_proj_client_name_fld1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_proj_client_name_fld1MouseClicked

    private void add_client_btn2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_client_btn2MouseClicked
        // TODO add your handling code here:
        this.add_link_proj_to_client();
    }//GEN-LAST:event_add_client_btn2MouseClicked

    private void add_otherid_btn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_otherid_btn1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_add_otherid_btn1MouseClicked

    private void valid_id_name_fld3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_valid_id_name_fld3MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_valid_id_name_fld3MouseClicked

    private void other_id_add_btn2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_other_id_add_btn2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_other_id_add_btn2MouseClicked

    private void other_id_add_btn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_other_id_add_btn2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_other_id_add_btn2ActionPerformed

    private void employee_position_name_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_employee_position_name_fldMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_employee_position_name_fldMouseClicked

    private void employee_position_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_employee_position_btnMouseClicked
        // TODO add your handling code here:
        this.add_employee_position();
        this.emp_position_jlist();
    }//GEN-LAST:event_employee_position_btnMouseClicked

    private void employee_position_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employee_position_btnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_employee_position_btnActionPerformed

    private void employee_position_rate_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_employee_position_rate_fldMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_employee_position_rate_fldMouseClicked

    private void add_other_id_delete1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_add_other_id_delete1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_add_other_id_delete1MouseClicked

    private void emp_pos_deleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emp_pos_deleteMouseClicked
        // TODO add your handling code here:
        
        String temp = this.emp_position_jlist.getSelectedValue().split(",")[0];
//        String temp = this.valid_other_id_list.getSe
        System.out.println(temp+" : temp");
        this.delete_emp_position_id(temp);
//        System.out.println(name);/
        this.emp_position_jlist();
        JOptionPane.showMessageDialog(null, "Successfully Deleted employee position!");
    }//GEN-LAST:event_emp_pos_deleteMouseClicked

    private void payslip_clear_deductions_fldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_payslip_clear_deductions_fldMouseClicked
        // TODO add your handling code here:
        this.payslip_ded_fld.setText("");
        this.payslipDeductionTblSelected="";
    }//GEN-LAST:event_payslip_clear_deductions_fldMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(mainwindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(mainwindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(mainwindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mainwindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new mainwindow().setVisible(true);
            }
        });
    }
    public void add_overtime(){
        
        try {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
//                  /--------------------
            Statement st = this.conn.createStatement();
            ResultSet res = st.executeQuery("SELECT * FROM tbl_emp_prof"); 
                  while (res.next()) { 
                    String id = res.getString("emp_id");
                    String msg = res.getString("emp_fname");
                    System.out.println(id + "\t" + msg);
            }
            
                  
            long timestamp = System.currentTimeMillis()/1000;
            String timestamp2 = Long.toString(timestamp);
            String sql = "INSERT INTO tbl_overtime (ovr_time_pay) VALUES (?)";
            PreparedStatement statement = this.conn.prepareStatement(sql);
            
//            statement.setString(1, this.ovr_time_id_fld.getText());
            statement.setString(1, this.ovr_time_rates_fld.getText());
            
            
            
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
            }
            
            this.conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     public void add_deduction(){
        
        try {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
         
//            long timestamp = System.currentTimeMillis()/1000;
//            String timestamp2 = Long.toString(timestamp);
            String sql = "INSERT INTO "+this.dbtables[2]+" (ded_name,ded_value) VALUES (?,?)";
            PreparedStatement statement = this.conn.prepareStatement(sql);
            statement.setString(1, this.ded_name_fld1.getText());
            statement.setString(2, this.ded_value_fld1.getText());
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
            }
 
        }catch(SQLException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    public void add_other_id(){
        try {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
            String sql = "INSERT INTO "+this.dbtables[4]+" (emp_ids_office_name) VALUES (?)";
            PreparedStatement statement = this.conn.prepareStatement(sql);
            statement.setString(1, this.valid_id_name_fld2.getText());
            statement.executeUpdate();
            System.out.println("pass here!");
//            if (rowsInserted > 0) {
//               System.out.println("A new user was inserted successfully!");
//            }
//            
        }catch(SQLException e){
            e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
    }
    public void update_client(){
        
        try {
            
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
//                  /--------------------
            long timestamp = System.currentTimeMillis()/1000;
            String today = Long.toString(timestamp);
            
            //From Jdatechooser to date format to 
        
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
            String date = sdf.format(this.emp_update_calendar.getDate());
            
            Date date2 = sdf.parse(date);
            //to timestamp
            long millis = date2.getTime()/1000;
            String bdayTimestamp = Long.toString(millis);
            
            String sql = "UPDATE "+this.dbtables[3]+" SET emp_id=?, emp_fname=?,emp_lname=?,emp_bday=?,emp_address=?, emp_standing=?, timestamp=? WHERE emp_id=?";
            PreparedStatement ps = this.conn.prepareStatement(sql);
            ps.setString(1,this.emp_update_emp_id_fld.getText());
            ps.setString(2, this.emp_update_fname_fld.getText());
            ps.setString(3, this.emp_update_lname_fld.getText());
            ps.setString(4, bdayTimestamp);
            ps.setString(5, this.emp_update_address_fld.getText());
            ps.setString(6, this.emp_update_standing_fld.getText());
            ps.setString(7, today);
            ps.setString(8, this._currentUpdateId);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Current employee record updated!");
            this.emp_update_emp_id_fld.setText("");
            this.emp_update_fname_fld.setText("");
            this.emp_update_lname_fld.setText("");

            this.emp_update_address_fld.setText("");
            this.emp_update_standing_fld.setText("");
//            }
            
        }catch(SQLException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
    }
    public void add_client2(){
         try {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
//                  /--------------------
     
            String sql = "INSERT INTO "+this.dbtables[0]+" (client_name, client_address) VALUES (?,?)";

            
            PreparedStatement statement = this.conn.prepareStatement(sql);
            statement.setString(1, this.proj_client_name_fld1.getText());
            statement.setString(2, this.proj_client_address_fld1.getText());
//            statement.executeUpdate();
            System.out.println("pass here!");
            
//            statement.executeUpdate();
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                JOptionPane.showMessageDialog(null, "Successfully Added Client");
            }
//            if (rowsInserted > 0) {
//                System.out.println("A new user was inserted successfully! "+this.proj_client_name_fld1.getText()+" ****"+this.proj_client_address_fld1.getText());
                
//            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                    this.proj_client_name_fld1.setText("");
                    this.proj_client_address_fld1.setText("");
                }
            }catch(Exception e){
            
            }
        }
    }
    
    public void linkProjectClient(){
        
    }
    public void add_proj(){
        
        try {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
//                  /--------------------
            Statement st = this.conn.createStatement();
//            ResultSet res = st.executeQuery("SELECT * FROM tbl_emp_prof"); 
//                  while (res.next()) { 
//                    String id = res.getString("emp_id");
//                    String msg = res.getString("emp_fname");
//                    System.out.println(id + "\t" + msg);
//            }
            long timestamp = System.currentTimeMillis()/1000;
            String today = Long.toString(timestamp);
            
            //From Jdatechooser to date format to 
        
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
            String date = sdf.format(this.add_proj_start_jchooser.getDate());
            Date startStamp = sdf.parse(date);
            String date1 = sdf.format(this.add_proj_end_jchooser.getDate());
            Date endStamp = sdf.parse(date1);
            //to timestamp
            long millis = startStamp.getTime()/1000;
            String startStamp2 = Long.toString(millis);
            long millis2 = endStamp.getTime()/1000;
            String endStamp2 = Long.toString(millis2);
       
            long timestamp2 = System.currentTimeMillis()/1000;
            String timestampRecord = Long.toString(timestamp2);
            
            String sql = "INSERT INTO tbl_project (prj_name,prj_start,prj_end, timestamp) VALUES (?,?,?,?)";
            PreparedStatement statement = this.conn.prepareStatement(sql);
            statement.setString(1, this.proj_name_fld.getText());
            statement.setString(2, startStamp2);
            statement.setString(3, endStamp2);
            statement.setString(4, timestampRecord);
            
            
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
            }
            
            this.conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    public void add_employee_position(){
        
        try {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
//                  /--------------------
//            Statement st = this.conn.createStatement();
//            ResultSet res = st.executeQuery("SELECT * FROM tbl_emp_prof"); 
//                  while (res.next()) { 
//                    String id = res.getString("emp_id");
//                    String msg = res.getString("emp_fname");
//                    System.out.println(id + "\t" + msg);
//            }
//            long timestamp = System.currentTimeMillis()/1000;
//            String timestamp2 = Long.toString(timestamp);
            String sql = "INSERT INTO tbl_position (pos_salary, pos_name) VALUES (?,?)";
            PreparedStatement st = this.conn.prepareStatement(sql);
            st.setDouble(1, Double.parseDouble(this.employee_position_rate_fld.getText()));
            st.setString(2, this.employee_position_name_fld.getText());
            
            
            int rowsInserted = st.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
            }
            
            this.conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
     public void add_other_proj(){
        
        try {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
//                  /--------------------
            Statement st = this.conn.createStatement();
//            ResultSet res = st.executeQuery("SELECT * FROM tbl_emp_prof"); 
//                  while (res.next()) { 
//                    String id = res.getString("emp_id");
//                    String msg = res.getString("emp_fname");
//                    System.out.println(id + "\t" + msg);
//            }
            long timestamp = System.currentTimeMillis()/1000;
            String timestamp2 = Long.toString(timestamp);
            String sql = "INSERT INTO tbl_emp_valid_ids (emp_ids_office_name) VALUES (?)";
            PreparedStatement statement = this.conn.prepareStatement(sql);
            statement.setString(1, this.valid_id_name_fld2.getText());
            
            
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
            }
            
            this.conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
      public void add_emp(){
        boolean temp = this.search_emp_id(this.emp_id_fld.getText());
        System.out.println(temp);
        if(temp){
            JOptionPane.showMessageDialog(null, "Duplicate entry!");
            return;
        }
        try {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            Statement st = this.conn.createStatement();
//            ResultSet res = st.executeQuery("SELECT * FROM tbl_emp_prof"); 
//                  while (res.next()) { 
//                    String id = res.getString("emp_id");
//                    String msg = res.getString("emp_fname");
//                    System.out.println(id + "\t" + msg);
//            }
//            String jcalendar = this.emp_bday_calendar.getDate().toString();
      
            System.out.println("Calendar value; ");
//            this.emp_bday_calendar.getD
           
            long timestamp = System.currentTimeMillis()/1000;
            String today = Long.toString(timestamp);
            
            //From Jdatechooser to date format to 
        
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
            String date = sdf.format(this.emp_bday_calendar.getDate());
            
            Date date2 = sdf.parse(date);
            //to timestamp
            long millis = date2.getTime()/1000;
            String bdayTimestamp = Long.toString(millis);
            
            String sql = "INSERT INTO 	tbl_emp_prof (emp_id, emp_lname,emp_fname,emp_bday,emp_address,emp_standing,timestamp) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement statement = this.conn.prepareStatement(sql);
            statement.setString(1, this.emp_id_fld.getText());
            statement.setString(2, this.emp_lname_fld.getText());
            statement.setString(3, this.emp_fname_fld.getText());
//            statement.setString(3, this.emp_bday_fld.getDate());
            statement.setString(4, bdayTimestamp);
            statement.setString(5, this.emp_add_fld.getText());
            statement.setString(6, this.emp_standing_fld.getText());
            statement.setString(7, today);
            
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
                JOptionPane.showMessageDialog(null, "Successfully added employee");
            }
            
            
        } 

        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try{
                if(conn!=null)
                this.conn.close();
            }catch(SQLException se){
               se.printStackTrace();
            }//end finally try
        }
        
    }
    public boolean search_emp_id(String searchid){
        boolean result=false;
        try {
            
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            String sql = "SELECT * FROM tbl_emp_prof WHERE emp_id=?";
            PreparedStatement st = this.conn.prepareStatement(sql);
            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            if(res.next()){
                result=true;
            }
            else {
                result=false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return result;
    }
    public String[] search_emp_id_with_result(String searchid){
        String[] result = new String[7];
        this._currentUpdateId = this.emp_update_emp_id_fld.getText();
        try {  
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            String sql = "SELECT * FROM tbl_emp_prof WHERE emp_id=?";
            PreparedStatement st = this.conn.prepareStatement(sql);
            st.setString(1, searchid);
            ResultSet res = st.executeQuery(); 
            while (res.next()) { 
                    result[0] = res.getString("emp_id");
                    
                    result[1]= res.getString("emp_lname");
                    result[2]= res.getString("emp_fname");
                    result[3]= res.getString("emp_bday");
                    result[4]= res.getString("emp_address");
                    result[5]= res.getString("emp_standing");
                    result[6]= res.getString("timestamp");
                    this.tempEmpData[0] = result[0];
                    this.tempEmpData[1] = result[1];
                    this.tempEmpData[2] = result[2];
                    this.tempEmpData[3] = result[3];
                    this.tempEmpData[4] = result[4];
                    this.tempEmpData[5] = result[5];
                    this.tempEmpData[6] = result[6];
//                    System.out.println(id + "\t" + msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return result;
    }
    public boolean emp_update_delete_id(String searchid){
        String[] result = new String[7];
        this._currentUpdateId = this.emp_update_emp_id_fld.getText();
        try {
            
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(this.url,this.userNamedb,this.passworddb);
//                  /--------------------
            String sql = "DELETE FROM "+this.dbtables[3]+" WHERE emp_id=?";
            PreparedStatement st = this.conn.prepareStatement(sql);
            st.setString(1, this._currentUpdateId);
            st.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Successfully remove employee record!");
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return true;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add_client_btn;
    private javax.swing.JLabel add_client_btn1;
    private javax.swing.JButton add_client_btn2;
    private javax.swing.JButton add_ded1;
    private javax.swing.JList<String> add_deduction_jlist;
    private javax.swing.JLabel add_deductions_btn;
    private javax.swing.JLabel add_empoyee_btn;
    private javax.swing.JLabel add_empoyee_btn2;
    private javax.swing.JScrollPane add_other_id_JList;
    private javax.swing.JScrollPane add_other_id_JList1;
    private javax.swing.JScrollPane add_other_id_JList2;
    private javax.swing.JButton add_other_id_delete;
    private javax.swing.JButton add_other_id_delete1;
    private javax.swing.JLabel add_otherid_btn;
    private javax.swing.JLabel add_otherid_btn1;
    private javax.swing.JList<String> add_overtime_jlist;
    private javax.swing.JLabel add_overtime_rate;
    private javax.swing.JButton add_proj_btn;
    private javax.swing.JComboBox<String> add_proj_combolist;
    private javax.swing.JComboBox<String> add_proj_combolist1;
    private com.toedter.calendar.JDateChooser add_proj_end_jchooser;
    private com.toedter.calendar.JDateChooser add_proj_start_jchooser;
    private javax.swing.JPanel body_main_panel;
    private javax.swing.JPanel client_panel;
    private javax.swing.JTextField ded_name_fld1;
    private javax.swing.JTextField ded_value_fld1;
    private javax.swing.JPanel deductions_panel;
    private javax.swing.JButton delete_click;
    private javax.swing.JButton delete_ded_btn;
    private javax.swing.JTextField emp_add_fld;
    private com.toedter.calendar.JDateChooser emp_bday_calendar;
    private javax.swing.JTextField emp_fname_fld;
    private javax.swing.JTextField emp_id_fld;
    private javax.swing.JTextField emp_lname_fld;
    private javax.swing.JButton emp_pos_delete;
    private javax.swing.JList<String> emp_position_jlist;
    private javax.swing.JTextField emp_standing_fld;
    private javax.swing.JTextField emp_update_address_fld;
    private com.toedter.calendar.JDateChooser emp_update_calendar;
    private javax.swing.JTextField emp_update_emp_id_fld;
    private javax.swing.JTextField emp_update_fname_fld;
    private javax.swing.JTextField emp_update_lname_fld;
    private javax.swing.JTextField emp_update_standing_fld;
    private javax.swing.JPanel employee_panel;
    private javax.swing.JButton employee_position_btn;
    private javax.swing.JTextField employee_position_name_fld;
    private javax.swing.JTextField employee_position_rate_fld;
    private javax.swing.JComboBox<String> employee_rec_link_empid_cmb;
    private javax.swing.JTextField gen_payslip_emp_hoursworked;
    private javax.swing.JTextField gen_payslip_emp_id_fld;
    private javax.swing.JTextField gen_payslip_emp_ot_thours;
    private javax.swing.JLabel generate_payslip_btn;
    private javax.swing.JButton generate_payslip_menu_btn;
    private javax.swing.JPanel header_panel;
    private javax.swing.JPanel hoverPanel_menu1_payslip;
    private javax.swing.JPanel hoverPanel_menu_clinet_project;
    private javax.swing.JPanel hoverPanel_menu_deductions;
    private javax.swing.JPanel hoverPanel_menu_employee;
    private javax.swing.JPanel hoverPanel_menu_other_id;
    private javax.swing.JPanel hoverPanel_menu_other_id1;
    private javax.swing.JPanel hoverPanel_menu_overtime;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox<String> jComboBox13;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator18;
    private javax.swing.JSeparator jSeparator19;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator20;
    private javax.swing.JSeparator jSeparator21;
    private javax.swing.JSeparator jSeparator22;
    private javax.swing.JSeparator jSeparator23;
    private javax.swing.JSeparator jSeparator24;
    private javax.swing.JSeparator jSeparator25;
    private javax.swing.JSeparator jSeparator26;
    private javax.swing.JSeparator jSeparator28;
    private javax.swing.JSeparator jSeparator29;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator30;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel menu;
    private javax.swing.JButton other_id_add_btn1;
    private javax.swing.JButton other_id_add_btn2;
    private javax.swing.JPanel otherid_panel;
    private javax.swing.JButton over_del_but;
    private javax.swing.JPanel overtime_panel;
    private javax.swing.JButton ovr_add_btn;
    private javax.swing.JTextField ovr_time_rates_fld;
    private javax.swing.JTable paylist_deduction_tbl;
    private javax.swing.JLabel payslip_clear_deductions_fld;
    private javax.swing.JComboBox<String> payslip_combox_client1;
    private javax.swing.JComboBox<String> payslip_combox_overtime1;
    private javax.swing.JComboBox<String> payslip_combox_position1;
    private javax.swing.JComboBox<String> payslip_combox_project1;
    private javax.swing.JTextField payslip_ded_fld;
    private javax.swing.JLabel payslip_deductions_res_fld;
    private javax.swing.JLabel payslip_fullname_lbl1;
    private javax.swing.JLabel payslip_grosspay_res_fld;
    private javax.swing.JLabel payslip_netpay_res_fld;
    private javax.swing.JPanel payslip_panel;
    private javax.swing.JTextField proj_client_address_fld1;
    private javax.swing.JTextField proj_client_name_fld1;
    private javax.swing.JTextField proj_name_fld;
    private javax.swing.JButton update_client;
    private javax.swing.JTextField username18;
    private javax.swing.JTextField valid_id_name_fld2;
    private javax.swing.JTextField valid_id_name_fld3;
    private javax.swing.JList<String> valid_other_id_list;
    private javax.swing.JList<String> valid_other_id_list1;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("C:\\Users\\maestrom4\\Documents\\NetBeansProjects\\payroll_sam_angel\\src\\payroll\\img\\icon.ico")));
//            Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("\\img\\icon.ico"));
//            ImageIcon icon = new ImageIcon( );
//            setIconImage(icon.getImage());

    }
    
    private void add_link_proj_to_client() {
        String tempDatalink_name = this.add_proj_combolist.getItemAt(this.add_proj_combolist.getSelectedIndex());
        try {
            int numero=0;
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url,userNamedb,passworddb);
//            
            /**
             * Insert to link client and projects
             */
            String sql2 = "INSERT INTO "+this.dbtables[1]+" (tbl_client_client_id,tbl_project_prj_id) VALUES (?,?)";
            PreparedStatement statement = this.conn.prepareStatement(sql2);
            String temp = this.add_proj_combolist.getItemAt(this.add_proj_combolist.getSelectedIndex()).split(",")[0];
            String temp2 = this.add_proj_combolist1.getItemAt(this.add_proj_combolist1.getSelectedIndex()).split(",")[0];
            statement.setString(1, this.add_proj_combolist.getItemAt(this.add_proj_combolist.getSelectedIndex()).split(",")[0]);
            statement.setString(2,  this.add_proj_combolist1.getItemAt(this.add_proj_combolist1.getSelectedIndex()).split(",")[0]);
//            statement.setString(3,  this.proj_client_lin_description_fld.getText());
            
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
                JOptionPane.showMessageDialog(null, "Successfully Link client to project");
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try{
                if(this.conn!=null){
                    this.conn.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private void dummyInitializer() {
//        this.add_link_proj_to_client();
    }
}
