/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 *
 * @author maestrom4
 */

//package com.mkyong;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.util.Date;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
public class pdfwriter2 {
    public String companyName = "";
    public String companyAddress = "";
    public String date = "";
    public String titleslip = "Salary Slip";
    public String employeeId = "";
    public String empFullName = "";
    public String clientName = "";
    public String listdeductions = "";
    public int hoursWorked = 0;
    public int overtimeHours = 0;
    public double overtimeRate = 0.0;
    public String postionRate = "";
    public int netpayId = 0;
    public String relativeFilenamepath="";
    public String computations = "";
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font catFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.BOLD);
//    public  String FILE_NAME = this.netpayId+this.employeeId+this.empFullName+".pdf";
    public  String FILE_NAME = "angelandsam.pdf";
    pdfwriter2(String cname, String companyAdd, String datep,String tslip,String empId,String clienN,String listded,int hWorked,int otHours,double otRate,String pRp, int netpayIdp, String empFullNamep, String empid2, String computep){
        
        this.companyName = cname;
        this.companyAddress = companyAdd;
        this.date = datep;
        this.titleslip = tslip;
        this.employeeId = empid2;
        this.clientName = clienN;
        this.listdeductions = listded;
        this.hoursWorked = hWorked;
        this.overtimeHours = otHours;
        this.overtimeRate = otRate;
        this.postionRate = pRp;
        this.netpayId = netpayIdp;
        this.empFullName = empFullNamep;
        this.relativeFilenamepath = datep+".pdf";
        this.computations = computep;
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
        String date = sdf.format(today);
        
        this.writeUsingIText(date,empFullNamep);
        
    }
//    private static String FILE = "c:/temp/FirstPdf.pdf";
    
//    public final String FILE_NAME = "C:\\Users\\maestrom4\\Documents\\NetBeansProjects\\payroll_sam_angel\\src\\payroll\\"+this.netpayId+this.employeeId+this.empFullName+".pdf";
    
//    public static void main(String[] args) {
//        writeUsingIText();
//        System.out.println();
//    }

    public void writeUsingIText(String date,String name) {
        Rectangle pageSize = new Rectangle(792, 512);
        Document document = new Document(pageSize);
        
        try {
//            File newfile = new File("");
            PdfWriter.getInstance(document, new FileOutputStream(new File(name+"-"+date+".pdf")));
//            Document document = new Document();
//            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addMetaData(document);
            addTitlePage(document);
            this.addContent(document,this.companyName);
            
            
        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        } 
//        catch (IOException e) {
//            e.printStackTrace();
//        }
        finally{
            try{
                if(document!=null){
                    document.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        

    }
    private static void addMetaData(Document document) {
        document.addTitle("My first PDF");
        document.addSubject("Using iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Lars Vogel");
        document.addCreator("Lars Vogel");
    }

    private static void addTitlePage(Document document)
            throws DocumentException {
//        Paragraph preface = new Paragraph();
//        // We add one empty line
//        addEmptyLine(preface, 1);
//        // Lets write a big header
//        preface.add(new Paragraph(companyName, catFont));
//
//        addEmptyLine(preface, 1);
        // Will create: Report generated by: _name, _date
//        preface.add(new Paragraph(
//                "Report generated by: " + System.getProperty("user.name") + ", " + new Date(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
//                smallBold));
//        addEmptyLine(preface, 3);
//        preface.add(new Paragraph(
//                "This document describes something which is very important ",
//                smallBold));

//        addEmptyLine(preface, 8);

//        preface.add(new Paragraph(
//                "This document is a preliminary version and not subject to your license agreement or any other agreement with vogella.com ;-).",
//                redFont));

//        document.add(preface);
        // Start a new page
//        document.newPage();
    }

    private void addContent(Document document, String companyName) throws DocumentException {
//        Anchor anchor = new Anchor(companyName, catFont);
//        anchor.setName(companyName);

        // Second parameter is the number of the chapter
//        Chapter catPart = new Chapter(new Paragraph(anchor), 1);
//        document.add(new Paragraph(companyName));
        Paragraph content = new Paragraph();
        content.add(new Paragraph(companyName, catFont));
    
        content.add(new Paragraph("["+this.titleslip+" #"+this.netpayId+"]", catFont2));
        content.add(new Paragraph(this.companyAddress, subFont));
        content.add(new Paragraph("Date: "+this.date, subFont));
//        // We add one empty line
        addEmptyLine(content, 1);
        content.add(new Paragraph("Employee ID: "+this.employeeId, subFont));
        content.add(new Paragraph("Employee Fullname:  "+this.empFullName, subFont));
        content.add(new Paragraph("Hours Worked: "+this.hoursWorked, subFont));
        content.add(new Paragraph("Overtime Hours: "+this.overtimeHours , subFont));
        content.add(new Paragraph("Overtime Rate: "+this.overtimeRate , subFont));
        String position = this.postionRate.split(",")[2];
        double posrate = Double.parseDouble(this.postionRate.split(",")[0]);
        content.add(new Paragraph("Position: "+position , subFont));
        content.add(new Paragraph("Position Rate: "+posrate , subFont));
        addEmptyLine(content, 1);
        content.add(createFirstTable(this.computations));
        addEmptyLine(content, 5);
        content.add(new Paragraph(
                "Report generated by: " + System.getProperty("user.name") + ", " + new Date(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                smallBold));
//        document.add();
        
        document.add(content);
      

     



    }

    public static PdfPTable createFirstTable(String tempData) {
    	// a table with three columns
        PdfPTable table = new PdfPTable(3);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        // t.setBorderColor(BaseColor.GRAY);
        // t.setPadding(4);
        // t.setSpacing(4);
        // t.setBorderWidth(1);
        String[] tempx = tempData.split(",");
        PdfPCell c1 = new PdfPCell(new Phrase("Total Deductions"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Gross Pay"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Net Pay"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.setHeaderRows(1);

        table.addCell(tempx[1]);
        table.addCell(tempx[0]);
        table.addCell(tempx[2]);
       
//        table.addCell("2.1");
//        table.addCell("2.2");
//        table.addCell("2.3");

  

        return table;
    }

    private static void createList(Section subCatPart) {
        List list = new List(true, false, 10);
        list.add(new ListItem("First point"));
        list.add(new ListItem("Second point"));
        list.add(new ListItem("Third point"));
        subCatPart.add(list);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}